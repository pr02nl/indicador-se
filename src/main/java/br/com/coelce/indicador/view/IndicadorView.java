/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.coelce.indicador.view;

import br.com.coelce.indicador.business.IndicadorBC;
import br.com.coelce.indicador.config.ViewConfig;
import br.com.coelce.indicador.exceptions.ValidationException;
import br.com.coelce.indicador.model.Indicador;
import br.gov.frameworkdemoiselle.exception.ExceptionHandler;
import br.gov.frameworkdemoiselle.stereotype.ViewController;
import java.util.ArrayList;
import java.util.Map;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.openswing.swing.client.GridControl;
import org.openswing.swing.export.java.ExportOptions;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOListResponse;
import org.openswing.swing.message.receive.java.VOResponse;
import org.openswing.swing.message.send.java.GridParams;
import org.openswing.swing.table.client.GridController;
import org.openswing.swing.table.java.GridDataLocator;

/**
 *
 * @author Paulo Roberto
 */
@ViewController
@Singleton
public class IndicadorView extends GridController implements GridDataLocator {

    @Inject
    private IndicadorBC indicadorBC;
    @Inject
    private ViewConfig config;
    private GridControl indicadorGridControl;
    private GridControl aquisicaoGridControl;

    public Response loadData(int action, int startIndex, Map filteredColumns,
            ArrayList currentSortedColumns, ArrayList currentSortedVersusColumns,
            Class valueObjectType, Map otherGridParams) {
        int count = indicadorBC.count(filteredColumns, otherGridParams);
        if (action == GridParams.PREVIOUS_BLOCK_ACTION) {
            action = GridParams.NEXT_BLOCK_ACTION;
            startIndex = Math.max(startIndex - config.getViewBlockSize(), 0);
        }
        return new VOListResponse(indicadorBC.findAll(filteredColumns, otherGridParams,
                new int[]{startIndex, config.getViewBlockSize()}, currentSortedColumns,
                currentSortedVersusColumns), count > startIndex + config.getViewBlockSize(), indicadorBC.findAll().size());
    }

    @Override
    public Response insertRecords(int[] rowNumbers, ArrayList newValueObjects) throws Exception {
        for (Object object : newValueObjects) {
            Indicador entity = (Indicador) object;
            indicadorBC.insert(entity);
        }
        return new VOListResponse(newValueObjects, false, newValueObjects.size());
    }

    @Override
    public Response updateRecords(int[] rowNumbers, ArrayList oldPersistentObjects, ArrayList persistentObjects) throws Exception {
        for (Object object : persistentObjects) {
            Indicador entity = (Indicador) object;
            indicadorBC.update(entity);
        }
        return new VOListResponse(persistentObjects, false, persistentObjects.size());
    }

    @Override
    public Response deleteRecords(ArrayList persistentObjects) throws Exception {
        for (Object object : persistentObjects) {
            Indicador entity = (Indicador) object;
            indicadorBC.delete(entity.getId());
        }
        return new VOResponse(true);
    }

    @Override
    public void rowChanged(int rowNumber) {
        if (rowNumber != -1 && getIndicadorGridControl() != null && getAquisicaoGridControl() != null) {
            Indicador indicadorForRow = (Indicador) getIndicadorGridControl().getVOListTableModel().getObjectForRow(rowNumber);
            getAquisicaoGridControl().getOtherGridParams().put("indicador", indicadorForRow);
            getAquisicaoGridControl().reloadData();
        }
    }

    @Override
    public String[] getExportingFormats() {
        return new String[]{ExportOptions.XLS_FORMAT};
    }

    @ExceptionHandler
    public void handleExceptions(ConstraintViolationException exception) {
        String errorMessage = "";
        for (ConstraintViolation<?> c : exception.getConstraintViolations()) {
            String msg = c.getMessage();
            if (msg.startsWith("{")) {
                msg = msg.substring(1, msg.lastIndexOf("}"));
            }
            //msg = Bundle.getString(msg);
            errorMessage += msg + "\n";
        }
        throw new ValidationException(errorMessage);
    }

    public GridControl getAquisicaoGridControl() {
        return aquisicaoGridControl;
    }

    public void setAquisicaoGridControl(GridControl aquisicaoGridControl) {
        this.aquisicaoGridControl = aquisicaoGridControl;
    }

    public GridControl getIndicadorGridControl() {
        return indicadorGridControl;
    }

    public void setIndicadorGridControl(GridControl indicadorGridControl) {
        this.indicadorGridControl = indicadorGridControl;
    }
}
