/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.coelce.indicador.graph;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.Date;
import java.util.List;
import javax.swing.JPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.IntervalMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.ui.Layer;

/**
 *
 * @author coelce
 */
public class TimeSeriesPanel extends JPanel {

    private JFreeChart jfreeChart;
    private TimeSeriesCollection timeCollection;
    private TimeSeries timeSeries;

    public TimeSeriesPanel() {
        //public static JFreeChart createTimeSeriesChart(String title,
        //String timeAxisLabel, String valueAxisLabel, XYDataset dataset,
        //boolean legend, boolean tooltips, boolean urls)
        this.setLayout(new BorderLayout());
        timeSeries = new TimeSeries("Serie1");
        timeCollection = new TimeSeriesCollection(timeSeries);
        jfreeChart = ChartFactory.createTimeSeriesChart("Titulo", "Time Label",
                "Valor Label", timeCollection, false, true, false);
        ChartPanel chartPanel = new ChartPanel(jfreeChart);
        add(chartPanel);
        jfreeChart.getXYPlot().getRangeAxis().setRange(40, 300);
//        jfreeChart.getXYPlot().getDomainMarkers(Layer.BACKGROUND);
    }

    public void setTitle(String title) {
        jfreeChart.setTitle(title);
    }

    public String getTitle() {
        return jfreeChart.getTitle().getText();
    }

    public void setTimeLabel(String timeLabel) {
        XYPlot xyPlot = jfreeChart.getXYPlot();
        xyPlot.getDomainAxis().setLabel(timeLabel);
    }

    public String getTimeLabel() {
        XYPlot xyPlot = jfreeChart.getXYPlot();
        return xyPlot.getDomainAxis().getLabel();
    }

    public void setValorLabel(String valueLabel) {
        XYPlot xyPlot = jfreeChart.getXYPlot();
        xyPlot.getRangeAxis().setLabel(valueLabel);
    }

    public String getValorLabel() {
        XYPlot xyPlot = jfreeChart.getXYPlot();
        return xyPlot.getRangeAxis().getLabel();
    }

    public void addValue(Date data, Double value) {
        timeSeries.add(new Millisecond(data), value);
    }

    public void addValue(String serie, Date data, Double value) {
        timeCollection.getSeries(serie).add(new Millisecond(data), value);
    }

    public void addSerie(String serie) {
        timeCollection.addSeries(new TimeSeries(serie));
    }

    public void removeSerie(String serie) {
        timeCollection.removeSeries(timeCollection.getSeries(serie));
    }

    public void clear() {
        List<TimeSeries> series = timeCollection.getSeries();
        for (TimeSeries serie : series) {
            serie.clear();
        }
    }

    public void addIntervalMarker(double start, double end, Color color, String label) {
        IntervalMarker intervalMarker = new IntervalMarker(start, end, color);
        jfreeChart.getXYPlot().addRangeMarker(intervalMarker, Layer.BACKGROUND);
    }
}
