/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.coelce.indicador.business;

import br.com.coelce.indicador.model.CentroServico;
import br.com.coelce.indicador.model.Departamento;
import br.com.coelce.indicador.persistence.CentroServicoDAO;
import br.com.pr02nl.business.MyDelegateCrud;
import br.gov.frameworkdemoiselle.stereotype.BusinessController;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Paulo
 */
@BusinessController
public class CentroServicoBC extends MyDelegateCrud<CentroServico, Long, CentroServicoDAO> {

    public List<CentroServico> findAllByDepartamento(Long departamentoId) {
        Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("departamento.id", departamentoId);
        return findAll(null, parametros, null, null);
    }

    public List<CentroServico> findAllByDepartamento(Departamento departamento) {
        Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("departamento", departamento);
        return findAll(null, parametros, null, null);
    }

    public CentroServico findBySigla(String stringTemp) {
        Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("sigla", stringTemp);
        List<CentroServico> findAll = findAll(null, parametros, null, null);
        if (!findAll.isEmpty()) {
            return findAll.get(0);
        }
        return null;
    }
}
