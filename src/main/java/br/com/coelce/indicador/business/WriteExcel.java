/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.coelce.indicador.business;

import br.com.coelce.indicador.model.Aquisicao;
import br.com.coelce.indicador.model.Medidas;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.util.List;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

/**
 *
 * @author Paulo Roberto
 */
public class WriteExcel {

    private Workbook work;
    private static WriteExcel instance;

    public static WriteExcel getInstance() throws FileNotFoundException {
        if (instance == null) {
            instance = new WriteExcel();
        }
        return instance;
    }

    private WriteExcel() throws FileNotFoundException {
        work = new HSSFWorkbook();
    }

    public boolean saveMedidas(List<Medidas> medidas, File file) throws IOException  {
        Sheet parametros = work.createSheet("Parametros");
        Sheet dados = work.createSheet("Dados(1)");
        CreationHelper creationHelper = work.getCreationHelper();
        geraSheetParametro(parametros, medidas.get(0).getAquisicao());
        dados.createRow(0).createCell(0).setCellValue(creationHelper.createRichTextString("SAGA4000 - ESB Electronic Services"));
        Row createRow = dados.createRow(1);
        for (int i = 1; i < 10; i++) {
            createRow.createCell(i + 2).setCellValue(creationHelper.createRichTextString("Canal 0" + i));
        }
        createRow = dados.createRow(2);
        createRow.createCell(0).setCellValue("Registro");
        createRow.createCell(1).setCellValue("Data");
        createRow.createCell(2).setCellValue("Hora");
        createRow.createCell(3).setCellValue("V1min");
        createRow.createCell(4).setCellValue("V1");
        createRow.createCell(5).setCellValue("V1max");
        createRow.createCell(6).setCellValue("V2min");
        createRow.createCell(7).setCellValue("V2");
        createRow.createCell(8).setCellValue("V2max");
        createRow.createCell(9).setCellValue("V3min");
        createRow.createCell(10).setCellValue("V3");
        createRow.createCell(11).setCellValue("V3max");
        createRow = dados.createRow(3);
        for (int i = 1; i < 10; i++) {
            createRow.createCell(i + 2).setCellValue(creationHelper.createRichTextString("( V)"));
        }
        int count = 4;
        for (Medidas medicao : medidas) {
            createRow = dados.createRow(count);
            createRow.createCell(0).setCellValue(count - 3);
            createRow.createCell(1).setCellValue(DateFormat.getDateInstance().format(medicao.getDataMedicao()));
            createRow.createCell(2).setCellValue(DateFormat.getTimeInstance().format(medicao.getDataMedicao()));
            createRow.createCell(3).setCellValue(medicao.getTensaoAmin());
            createRow.createCell(4).setCellValue(medicao.getTensaoA());
            createRow.createCell(5).setCellValue(medicao.getTensaoAmax());
            createRow.createCell(6).setCellValue(medicao.getTensaoBmin());
            createRow.createCell(7).setCellValue(medicao.getTensaoB());
            createRow.createCell(8).setCellValue(medicao.getTensaoBmax());
            createRow.createCell(9).setCellValue(medicao.getTensaoCmin());
            createRow.createCell(10).setCellValue(medicao.getTensaoC());
            createRow.createCell(11).setCellValue(medicao.getTensaoCmax());
            count++;
        }
        FileOutputStream fileOut = new FileOutputStream(file);
        work.write(fileOut);
        fileOut.close();
        fileOut = null;
        return true;
    }

    private void geraSheetParametro(Sheet parametros, Aquisicao aquisicao) {
        Row createRow = parametros.createRow(0);
        createRow.createCell(0).setCellValue("SAGA4000 - ESB Electronic Services");
        createRow.createCell(5).setCellValue(4000);
        createRow.createCell(6).setCellValue(0);
        createRow.createCell(7).setCellValue(48);
        createRow = parametros.createRow(1);
        createRow.createCell(4).setCellValue("Versão Leitor:");
        createRow.createCell(6).setCellValue(0);
        createRow.createCell(7).setCellValue(48);
        createRow = parametros.createRow(2);
        createRow.createCell(0).setCellValue("Parâmetros do medidor:");
        createRow.createCell(5).setCellValue("Informações sobre o Local:");
        createRow = parametros.createRow(3);
        createRow.createCell(0).setCellValue("Hora da Leitura:");
        createRow.createCell(3).setCellValue(DateFormat.getTimeInstance().format(aquisicao.getDataAquisicao()));
        createRow.createCell(5).setCellValue("Cod.Local:");
        createRow.createCell(6).setCellValue(aquisicao.getIndicador().getFriendlyName());
        createRow = parametros.createRow(4);
        createRow.createCell(0).setCellValue("Data da Leitura:");
        createRow.createCell(3).setCellValue(DateFormat.getDateInstance().format(aquisicao.getDataAquisicao()));
        createRow = parametros.createRow(5);
        createRow.createCell(0).setCellValue("Relação de TP:");
        createRow.createCell(3).setCellValue("1");
        createRow.createCell(5).setCellValue("Totalizadores de Energia");
        createRow = parametros.createRow(6);
        createRow.createCell(0).setCellValue("Relação de TC:");
        createRow.createCell(3).setCellValue("1");
        createRow.createCell(5).setCellValue("Ativa Dir.");
        createRow.createCell(6).setCellValue(0);
        createRow.createCell(7).setCellValue("(Wh)");
        createRow = parametros.createRow(7);
        createRow.createCell(0).setCellValue("Intervalo de Integração:");
        createRow.createCell(3).setCellValue(600);
        createRow.createCell(4).setCellValue("(s)");
        createRow.createCell(5).setCellValue("Ind. Dir.");
        createRow.createCell(6).setCellValue(0);
        createRow.createCell(7).setCellValue("(varh)");
        createRow = parametros.createRow(8);
        createRow.createCell(0).setCellValue("Versão:");
        createRow.createCell(3).setCellValue("33.64");
        createRow.createCell(5).setCellValue("Cap. Dir.");
        createRow.createCell(6).setCellValue(0);
        createRow.createCell(7).setCellValue("(varh)");
        createRow = parametros.createRow(9);
        createRow.createCell(0).setCellValue("Número de Série:");
        createRow.createCell(3).setCellValue(aquisicao.getIndicador().getFriendlyName());
        createRow.createCell(5).setCellValue("Ativa Rev.");
        createRow.createCell(6).setCellValue(0);
        createRow.createCell(7).setCellValue("(Wh)");
        createRow = parametros.createRow(10);
        createRow.createCell(0).setCellValue("Tempo de QR:");
        createRow.createCell(3).setCellValue(2);
        createRow.createCell(4).setCellValue("(s)");
        createRow.createCell(5).setCellValue("Ind Rev.");
        createRow.createCell(6).setCellValue(0);
        createRow.createCell(7).setCellValue("(varh)");
        createRow = parametros.createRow(11);
        createRow.createCell(5).setCellValue("Cap Rev.");
        createRow.createCell(6).setCellValue(0);
        createRow.createCell(7).setCellValue("(varh)");
        parametros.createRow(12).createCell(0).setCellValue("Configuração dos Canais:");
        createRow = parametros.createRow(13);
        createRow.createCell(0).setCellValue(9);
        createRow.createCell(1).setCellValue("Canais Ativos");
        createRow = parametros.createRow(14);
        createRow.createCell(0).setCellValue(2);
        createRow.createCell(1).setCellValue("Canal 01 - Tensao Minima V1");
        createRow = parametros.createRow(15);
        createRow.createCell(0).setCellValue(3);
        createRow.createCell(1).setCellValue("Canal 02 - Tensao V1");
        createRow = parametros.createRow(16);
        createRow.createCell(0).setCellValue(4);
        createRow.createCell(1).setCellValue("Canal 03 - Tensao Maxima V1");
        createRow = parametros.createRow(17);
        createRow.createCell(0).setCellValue(5);
        createRow.createCell(1).setCellValue("Canal 04 - Tensao Minima V2");
        createRow = parametros.createRow(18);
        createRow.createCell(0).setCellValue(6);
        createRow.createCell(1).setCellValue("Canal 05 - Tensao V2");
        createRow = parametros.createRow(19);
        createRow.createCell(0).setCellValue(7);
        createRow.createCell(1).setCellValue("Canal 06 - Tensao Maxima V2");
        createRow = parametros.createRow(20);
        createRow.createCell(0).setCellValue(8);
        createRow.createCell(1).setCellValue("Canal 07 - Tensao Minima V3");
        createRow = parametros.createRow(21);
        createRow.createCell(0).setCellValue(9);
        createRow.createCell(1).setCellValue("Canal 08 - Tensao V3");
        createRow = parametros.createRow(22);
        createRow.createCell(0).setCellValue(10);
        createRow.createCell(1).setCellValue("Canal 09 - Tensao Maxima V3");
    }
}
