/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.coelce.indicador.facade;

import br.com.coelce.indicador.frame.ConectaSerialFrame;
import br.com.coelce.indicador.frame.IndicadorFrame;
import br.com.coelce.indicador.frame.SearchFrame;
import br.gov.frameworkdemoiselle.stereotype.FacadeController;
import br.gov.frameworkdemoiselle.util.Beans;
import javax.inject.Singleton;
import org.openswing.swing.mdi.client.ClientFacade;
import org.openswing.swing.mdi.client.MDIFrame;

/**
 *
 * @author Paulo Roberto
 */
@FacadeController
@Singleton
public class IndicadorFacade implements ClientFacade {

    /*
     * @Inject private ClienteFrame clienteFrame; @Inject private
     * ProjetistaFrame projetistaFrame; @Inject private CidadeFrame cidadeFrame;
     * @Inject private AnalistaFrame analistaFrame;
     */
    public void showSearchFrame() {
        MDIFrame.add(Beans.getReference(SearchFrame.class));
    }

    public void showSerialFrame() {
        MDIFrame.add(Beans.getReference(ConectaSerialFrame.class));
    }

    public void showIndicadoresFrame() {
        MDIFrame.add(Beans.getReference(IndicadorFrame.class));
    }

    public void updateFirmware() {
    }
}
