/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.coelce.indicador.view;

import br.gov.frameworkdemoiselle.stereotype.ViewController;
import java.io.Serializable;
import javax.inject.Inject;
import org.openswing.swing.form.client.FormController;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOResponse;

/**
 *
 * @author Paulo Roberto
 */
@ViewController
public class DrpDrcView extends FormController implements Serializable {

    @Inject
    private AquisicaoView aquisicaoView;

    @Override
    public Response loadData(Class valueObjectClass, Object pkKey) {
        return new VOResponse(aquisicaoView.getCurrentAquisicao());
    }

}
