/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.coelce.indicador.persistence;

import br.com.coelce.indicador.model.Indicador;
import br.com.pr02nl.model.persistence.DaoGenerico;
import br.gov.frameworkdemoiselle.stereotype.PersistenceController;

/**
 *
 * @author Paulo Roberto
 */
@PersistenceController
public class IndicadorDAO extends DaoGenerico<Indicador, Long> {
}
