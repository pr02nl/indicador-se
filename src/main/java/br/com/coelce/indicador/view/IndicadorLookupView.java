/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.coelce.indicador.view;

import br.com.coelce.indicador.business.IndicadorBC;
import br.com.coelce.indicador.config.ViewConfig;
import br.gov.frameworkdemoiselle.stereotype.ViewController;
import java.util.ArrayList;
import java.util.Map;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeModel;
import org.openswing.swing.lookup.client.LookupDataLocator;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOListResponse;
import org.openswing.swing.message.receive.java.VOResponse;
import org.openswing.swing.message.send.java.GridParams;
import org.openswing.swing.tree.java.OpenSwingTreeNode;

/**
 *
 * @author Paulo Roberto
 */
@ViewController
@Singleton
public class IndicadorLookupView extends LookupDataLocator {

    @Inject
    private IndicadorBC indicadorBC;
    @Inject
    private ViewConfig config;

    @Override
    public Response loadData(int action, int startIndex, Map filteredColumns, ArrayList currentSortedColumns, ArrayList currentSortedVersusColumns, Class valueObjectType) {
        int count = indicadorBC.count(filteredColumns,null);
        if (action == GridParams.PREVIOUS_BLOCK_ACTION) {
            action = GridParams.NEXT_BLOCK_ACTION;
            startIndex = Math.max(startIndex - config.getViewBlockSize(), 0);
        }
        return new VOListResponse(indicadorBC.findAll(filteredColumns, null,
                new int[]{startIndex, config.getViewBlockSize()}, currentSortedColumns,
                currentSortedVersusColumns), count > startIndex + config.getViewBlockSize(), indicadorBC.findAll().size());
    }

    @Override
    public Response validateCode(String code) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Response getTreeModel(JTree tree) {
        return new VOResponse(new DefaultTreeModel(new OpenSwingTreeNode()));
    }
}
