/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.coelce.indicador.model;

import javax.persistence.Entity;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 *
 * @author pr02nl
 */
@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Departamento extends BaseModel {

    private String nome;
    private String gerencia;
    private String ceco;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getGerencia() {
        return gerencia;
    }

    public void setGerencia(String gerencia) {
        this.gerencia = gerencia;
    }

    public String getCeco() {
        return ceco;
    }

    public void setCeco(String ceco) {
        this.ceco = ceco;
    }

    @Override
    public String toString() {
        return getNome();
    }
}
