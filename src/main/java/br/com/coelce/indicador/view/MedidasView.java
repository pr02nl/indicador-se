/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.coelce.indicador.view;

import br.com.coelce.indicador.business.MedidasBC;
import br.com.coelce.indicador.config.ViewConfig;
import br.com.coelce.indicador.exceptions.ValidationException;
import br.com.coelce.indicador.model.Medidas;
import br.gov.frameworkdemoiselle.exception.ExceptionHandler;
import br.gov.frameworkdemoiselle.stereotype.ViewController;
import java.util.ArrayList;
import java.util.Map;
import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOListResponse;
import org.openswing.swing.message.receive.java.VOResponse;
import org.openswing.swing.message.send.java.GridParams;
import org.openswing.swing.table.client.GridController;
import org.openswing.swing.table.java.GridDataLocator;

/**
 *
 * @author Paulo Roberto
 */
@ViewController
public class MedidasView extends GridController implements GridDataLocator {

    @Inject
    private MedidasBC medidasBC;
    @Inject
    private ViewConfig config;
    @Inject
    private AquisicaoView aquisicaoView;

    public Response loadData(int action, int startIndex, Map filteredColumns,
            ArrayList currentSortedColumns, ArrayList currentSortedVersusColumns,
            Class valueObjectType, Map otherGridParams) {
        int count = medidasBC.count(filteredColumns, otherGridParams);
        if (action == GridParams.PREVIOUS_BLOCK_ACTION) {
            action = GridParams.NEXT_BLOCK_ACTION;
            startIndex = Math.max(startIndex - config.getViewBlockSize(), 0);
        }
        if (!otherGridParams.containsKey("aquisicao")) {
            otherGridParams.put("aquisicao", aquisicaoView.getCurrentAquisicao());
        }
        return new VOListResponse(medidasBC.findAll(filteredColumns, otherGridParams,
                new int[]{startIndex, config.getViewBlockSize()}, currentSortedColumns,
                currentSortedVersusColumns), count > startIndex + config.getViewBlockSize(), medidasBC.findAll().size());
    }

    @Override
    public Response insertRecords(int[] rowNumbers, ArrayList newValueObjects) throws Exception {
        for (Object object : newValueObjects) {
            Medidas entity = (Medidas) object;
            medidasBC.insert(entity);
        }
        return new VOListResponse(newValueObjects, false, newValueObjects.size());
    }

    @Override
    public Response updateRecords(int[] rowNumbers, ArrayList oldPersistentObjects, ArrayList persistentObjects) throws Exception {
        for (Object object : persistentObjects) {
            Medidas entity = (Medidas) object;
            medidasBC.update(entity);
        }
        return new VOListResponse(persistentObjects, false, persistentObjects.size());
    }

    @Override
    public Response deleteRecords(ArrayList persistentObjects) throws Exception {
        for (Object object : persistentObjects) {
            Medidas entity = (Medidas) object;
            medidasBC.delete(entity.getId());
        }
        return new VOResponse(true);
    }

    @ExceptionHandler
    public void handleExceptions(ConstraintViolationException exception) {
        String errorMessage = "";
        for (ConstraintViolation<?> c : exception.getConstraintViolations()) {
            String msg = c.getMessage();
            if (msg.startsWith("{")) {
                msg = msg.substring(1, msg.lastIndexOf("}"));
            }
            //msg = Bundle.getString(msg);
            errorMessage += msg + "\n";
        }
        throw new ValidationException(errorMessage);
    }
}
