/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.coelce.indicador.business;

import br.com.coelce.indicador.model.Indicador;
import br.com.coelce.indicador.persistence.IndicadorDAO;
import br.com.pr02nl.business.MyDelegateCrud;
import br.gov.frameworkdemoiselle.stereotype.BusinessController;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Paulo Roberto
 */
@BusinessController
public class IndicadorBC extends MyDelegateCrud<Indicador, Long, IndicadorDAO> {

    public Indicador findByBluetoothAddress(String bluetoothAddress) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("bluetoothAddress", bluetoothAddress);
        List<Indicador> findAll = findAll(new HashMap<String, Object>(), params, null, null);
        if (findAll == null || findAll.isEmpty()) {
            return null;
        }
        return findAll.get(0);
    }
}
