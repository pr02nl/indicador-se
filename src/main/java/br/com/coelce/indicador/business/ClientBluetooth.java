/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.coelce.indicador.business;

import br.com.coelce.indicador.model.Indicador;
import br.gov.frameworkdemoiselle.stereotype.BusinessController;
import java.io.IOException;
import java.util.*;
import javax.bluetooth.*;
import javax.bluetooth.UUID;
import net.wimpi.modbus.io.ModbusBlueToothTransaction;
import net.wimpi.modbus.io.ModbusTransaction;
import net.wimpi.modbus.net.BlueToothConnection;
import net.wimpi.modbus.net.Connection;

/**
 *
 * @author Paulo Roberto
 */
@BusinessController
public class ClientBluetooth extends ClienteIndicador {

    private final InquiryListener inq_listener = new InquiryListener();
    private BlueToothConnection connection;
    private ModbusBlueToothTransaction transaction;
    private ServiceRecord service;
    private ArrayList<Indicador> dispositivos;

    private void search() throws BluetoothStateException, InterruptedException {
        LocalDevice local_device = LocalDevice.getLocalDevice();
        DiscoveryAgent disc_agent = local_device.getDiscoveryAgent();
        if (local_device.getDiscoverable() != DiscoveryAgent.LIAC) {
            local_device.setDiscoverable(DiscoveryAgent.LIAC);
        }
        synchronized (inq_listener) {
            inq_listener.cached_devices.clear();
            inq_listener.cached_services.clear();
            disc_agent.startInquiry(DiscoveryAgent.LIAC, inq_listener);
            inq_listener.wait();
        }
        Iterator devices = inq_listener.cached_devices.iterator();
        UUID[] u = new UUID[]{new UUID(0x0003)};
        int attrbs[] = {0x0100};
        while (devices.hasNext()) {
            synchronized (inq_listener) {
                disc_agent.searchServices(attrbs, u, (RemoteDevice) devices.next(), inq_listener);
                inq_listener.wait();
            }
        }
    }

    public List<Indicador> getDispositivos() throws IOException, InterruptedException {
        if (dispositivos == null) {
            search();
            dispositivos = new ArrayList<Indicador>(inq_listener.cached_services.size());
            for (int i = 0; i < inq_listener.cached_services.size(); i++) {
                ServiceRecord sr = (ServiceRecord) inq_listener.cached_services.get(i);
                dispositivos.add(new Indicador(sr.getHostDevice().getFriendlyName(true), sr.getHostDevice().getBluetoothAddress()));
            }
        }
        return dispositivos;
    }

    public String getRemoteName() throws IOException {
        if (service != null) {
            return service.getHostDevice().getFriendlyName(false);
        }
        return null;
    }

    public String getRemoteAddress() {
        if (service != null) {
            return service.getHostDevice().getBluetoothAddress();
        }
        return null;
    }

    @Override
    public void disconnect() throws IOException {
        super.disconnect();
        service = null;
        dispositivos = null;
    }

    @Override
    public int connect(int index) throws Exception {
        service = (ServiceRecord) inq_listener.getCached_services().get(index);
        if (service != null) {
            setConnection(new BlueToothConnection(service));
            getConnection().open();
            setTransaction(new ModbusBlueToothTransaction((BlueToothConnection) getConnection()));
            return firstConect();
        }
        return -1;
    }
//    public int connect(int index) throws Exception {
//        service = (ServiceRecord) inq_listener.getCached_services().get(index);
//        if (service != null) {
//            con = new BlueToothConnection(service);
//            ReadInputRegistersRequest req = new ReadInputRegistersRequest(CALIBRADO, 2);
//            req.setUnitID(0);
//            if (!con.isOpen()) {
//                con.open();
//            }
//            trans = new ModbusBlueToothTransaction(con);
//            trans.setRequest(req);
//            trans.execute();
//            ReadInputRegistersResponse resp = (ReadInputRegistersResponse) trans.getResponse();
//            unitID = resp.getUnitID();
//            calibrado = resp.getRegister(0).toBytes()[0];
//            if (calibrado == 0) {
//                updateRegistros();
//            }
//            getMedindo();
//        }
//        return calibrado;
//    }

    public void writeLineFirmware(String line) {
        getTransaction().setRequest(null);
    }

    @Override
    public Connection getConnection() {
        return connection;
    }

    @Override
    public void setConnection(Connection con) {
        this.connection = (BlueToothConnection) con;
    }

    @Override
    public ModbusTransaction getTransaction() {
        return transaction;
    }

    @Override
    public void setTransaction(ModbusTransaction trans) {
        this.transaction = (ModbusBlueToothTransaction) trans;
    }

    class InquiryListener implements DiscoveryListener {

        private List<RemoteDevice> cached_devices;
        private List<ServiceRecord> cached_services;

        public InquiryListener() {
            cached_devices = new ArrayList<RemoteDevice>();
            cached_services = new ArrayList<ServiceRecord>();
        }

        @Override
        public void deviceDiscovered(RemoteDevice btDevice, DeviceClass cod) {
            if (!cached_devices.contains(btDevice)) {
                cached_devices.add(btDevice);
            }
        }

        @Override
        public void inquiryCompleted(int discType) {
            synchronized (this) {
                this.notify();
            }
        }

        @Override
        public void servicesDiscovered(int transID, ServiceRecord[] servRecord) {
            if (!cached_services.contains(servRecord[0])) {
                cached_services.add(servRecord[0]);
            }
        }

        @Override
        public void serviceSearchCompleted(int transID, int respCode) {
            synchronized (this) {
                this.notify();
            }
        }

        public List<RemoteDevice> getCached_devices() {
            return cached_devices;
        }

        public List<ServiceRecord> getCached_services() {
            return cached_services;
        }
    }
}
