/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.coelce.indicador.view;

import br.com.coelce.indicador.business.AquisicaoBC;
import br.com.coelce.indicador.business.ClientBluetooth;
import br.com.coelce.indicador.business.IndicadorBC;
import br.com.coelce.indicador.business.MedidasBC;
import br.com.coelce.indicador.model.Aquisicao;
import br.com.coelce.indicador.model.Indicador;
import br.com.coelce.indicador.model.Medidas;
import br.com.coelce.indicador.model.VOMedidas;
import br.gov.frameworkdemoiselle.stereotype.ViewController;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.inject.Singleton;
import net.wimpi.modbus.ModbusException;
import net.wimpi.modbus.procimg.MedicaoRegister;
import org.apache.commons.math.MathException;
import org.openswing.swing.form.client.FormController;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOResponse;
import org.openswing.swing.miscellaneous.client.ProgressDialog;
import org.openswing.swing.miscellaneous.client.ProgressEvent;

/**
 *
 * @author Paulo Roberto
 */
@ViewController
@Singleton
public class MedicaoView extends FormController {

    @Inject
    private ClientBluetooth bluetooth;
    @Inject
    private IndicadorBC indicadorBC;
    @Inject
    private AquisicaoBC aquisicaoBC;
    @Inject
    private MedidasBC medidasBC;
    private ProgressDialog dialog;
    private VOMedidas medida = new VOMedidas();

    @Override
    public Response loadData(Class valueObjectClass, Object pkKey) {
        try {
            MedicaoRegister lerTensao = bluetooth.lerTensao(0, 1);
            medida.setDataMedicao(bluetooth.lerData());
            medida.setTensaoA(bluetooth.calculaTensaoA(lerTensao.getTensaoA()));
            medida.setTensaoB(bluetooth.calculaTensaoB(lerTensao.getTensaoB()));
            medida.setTensaoC(bluetooth.calculaTensaoC(lerTensao.getTensaoC()));
            medida.setNumRegistros(bluetooth.getCountRegisters());
            medida.setMedindo(bluetooth.getMedindo() != 0);
            return new VOResponse(medida);
        } catch (MathException ex) {
            return new ErrorResponse(ex.getLocalizedMessage());
        } catch (ModbusException ex) {
            return new ErrorResponse(ex.getLocalizedMessage());
        }
    }

    public VOMedidas getMedida() {
        return medida;
    }

    public void updateDrpDrc(int countRegisters, Aquisicao values) throws ModbusException {
        int[] contadoresA = bluetooth.lerMemoria(ClientBluetooth.CONTADORES_A, 12);
        int[] contadoresB = bluetooth.lerMemoria(ClientBluetooth.CONTADORES_B, 12);
        int[] contadoresC = bluetooth.lerMemoria(ClientBluetooth.CONTADORES_C, 12);

        values.setDrpFaseA24(contadoresA[0] * 0.6944444444444444);
        values.setDrpFaseA72(contadoresA[1] * 0.2314814814814815);
        values.setDrpFaseA168(contadoresA[2] * 0.0992063492063492);

        values.setDrpFaseB24(contadoresB[0] * 0.6944444444444444);
        values.setDrpFaseB72(contadoresB[1] * 0.2314814814814815);
        values.setDrpFaseB168(contadoresB[2] * 0.0992063492063492);

        values.setDrpFaseC24(contadoresC[0] * 0.6944444444444444);
        values.setDrpFaseC72(contadoresC[1] * 0.2314814814814815);
        values.setDrpFaseC168(contadoresC[2] * 0.0992063492063492);

        values.setDrcFaseA24(contadoresA[3] * 0.6944444444444444);
        values.setDrcFaseA72(contadoresA[4] * 0.2314814814814815);
        values.setDrcFaseA168(contadoresA[5] * 0.0992063492063492);

        values.setDrcFaseB24(contadoresB[3] * 0.6944444444444444);
        values.setDrcFaseB72(contadoresB[4] * 0.2314814814814815);
        values.setDrcFaseB168(contadoresB[5] * 0.0992063492063492);

        values.setDrcFaseC24(contadoresC[3] * 0.6944444444444444);
        values.setDrcFaseC72(contadoresC[4] * 0.2314814814814815);
        values.setDrcFaseC168(contadoresC[5] * 0.0992063492063492);

        if (countRegisters < 1008) {
            values.setDrpFaseA168(-1);
            values.setDrpFaseB168(-1);
            values.setDrpFaseC168(-1);
            values.setDrcFaseA168(-1);
            values.setDrcFaseB168(-1);
            values.setDrcFaseC168(-1);
        }
        if (countRegisters < 432) {
            values.setDrpFaseA72(-1);
            values.setDrpFaseB72(-1);
            values.setDrpFaseC72(-1);
            values.setDrcFaseA72(-1);
            values.setDrcFaseB72(-1);
            values.setDrcFaseC72(-1);
        }
    }

    public ProgressDialog getDialog() {
        return dialog;
    }

    public void setDialog(ProgressDialog dialog) {
        this.dialog = dialog;
    }

    public void startDownload(ProgressDialog dialog) {
        setDialog(dialog);
        new Thread(new DownloadMedicas()).start();
        dialog.setVisible(true);
    }

    public void stopMedicao() throws Exception {
        bluetooth.writeRegister(ClientBluetooth.MEDIR, 0);
    }

    public void startMedicao() throws Exception {
        bluetooth.writeRegister(ClientBluetooth.MEDIR, 1);
    }

    public void startMedicao(Date time) throws Exception {
        bluetooth.setData(time, 1);
    }

    private class DownloadMedicas implements Runnable {

        @Override
        public void run() {
            try {
                Indicador findByBluetoothAddress = indicadorBC.findByBluetoothAddress(bluetooth.getRemoteAddress());
                if (findByBluetoothAddress == null) {
                    indicadorBC.insert(new Indicador(bluetooth.getRemoteName(), bluetooth.getRemoteAddress()));
                    findByBluetoothAddress = indicadorBC.findByBluetoothAddress(bluetooth.getRemoteAddress());
                } else {
                    findByBluetoothAddress.setFriendlyName(bluetooth.getRemoteName());
                    indicadorBC.update(findByBluetoothAddress);
                }
                Aquisicao aquisicao = new Aquisicao();
                aquisicao.setDataAquisicao(Calendar.getInstance().getTime());
                aquisicao.setIndicador(findByBluetoothAddress);
                int countRegisters = bluetooth.getCountRegisters();
                if (countRegisters == 0xFFFF) {
                    throw new IOException("Indicador sem amostras!");
                }
                getDialog().setMaximumValue(countRegisters);
                getDialog().processProgressEvent(new ProgressEvent(0, "progressTitle", "progressMessage", new String[]{"updateDRPDRC"}));
                updateDrpDrc(countRegisters, aquisicao);
                aquisicaoBC.insert(aquisicao);
                Medidas medida = null;
                for (int i = 0; i < countRegisters; i++) {
                    MedicaoRegister lerTensao = bluetooth.lerTensao(i, 0);
                    medida = new Medidas();
                    medida.setDataMedicao(lerTensao.getDataMedicao());
                    medida.setTensaoA(bluetooth.calculaTensaoA(lerTensao.getTensaoA()));
                    medida.setTensaoB(bluetooth.calculaTensaoB(lerTensao.getTensaoB()));
                    medida.setTensaoC(bluetooth.calculaTensaoC(lerTensao.getTensaoC()));
                    medida.setTensaoAmin(bluetooth.calculaTensaoA(lerTensao.getTensaoAmin()));
                    medida.setTensaoBmin(bluetooth.calculaTensaoB(lerTensao.getTensaoBmin()));
                    medida.setTensaoCmin(bluetooth.calculaTensaoC(lerTensao.getTensaoCmin()));
                    medida.setTensaoAmax(bluetooth.calculaTensaoA(lerTensao.getTensaoAmax()));
                    medida.setTensaoBmax(bluetooth.calculaTensaoB(lerTensao.getTensaoBmax()));
                    medida.setTensaoCmax(bluetooth.calculaTensaoC(lerTensao.getTensaoCmax()));
                    medida.setAquisicao(aquisicao);
                    medidasBC.insert(medida);
                    getDialog().processProgressEvent(new ProgressEvent(i + 1, "progressTitle", "progressMessage", new String[]{"baixando"}));
                }
            } catch (MathException ex) {
                Logger.getLogger(MedicaoView.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ModbusException ex) {
                Logger.getLogger(MedicaoView.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(MedicaoView.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
