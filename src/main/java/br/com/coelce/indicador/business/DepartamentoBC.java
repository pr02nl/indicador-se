/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.coelce.indicador.business;

import br.com.coelce.indicador.model.Departamento;
import br.com.coelce.indicador.persistence.DepartamentoDAO;
import br.com.pr02nl.business.MyDelegateCrud;
import br.gov.frameworkdemoiselle.stereotype.BusinessController;

/**
 *
 * @author pr02nl
 */
@BusinessController
public class DepartamentoBC extends MyDelegateCrud<Departamento, Long, DepartamentoDAO> {
}
