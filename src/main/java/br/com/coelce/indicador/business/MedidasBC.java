/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.coelce.indicador.business;

import br.com.coelce.indicador.model.Aquisicao;
import br.com.coelce.indicador.model.Medidas;
import br.com.coelce.indicador.persistence.MedidasDAO;
import br.com.pr02nl.business.MyDelegateCrud;
import br.gov.frameworkdemoiselle.stereotype.BusinessController;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Paulo Roberto
 */
@BusinessController
public class MedidasBC extends MyDelegateCrud<Medidas, Long, MedidasDAO> {

    public int countByAquisicao(Aquisicao aquisicao) {
        Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("aquisicao", aquisicao);
        return count(null, parametros);
    }

    public List<Medidas> findAllByAquisicao(Aquisicao aquisicao) {
        Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("aquisicao", aquisicao);
        return findAll(null, parametros, null, null);
    }

    public int deleteAllByAquisicao(Aquisicao aquisicao) {
        return getDelegate().deleteAllByAquisicao(aquisicao);
    }
}
