/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.coelce.indicador.business;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import javax.microedition.io.Connector;

/**
 *
 * @author Paulo Roberto
 */
public class DpromConnection {

    private boolean conectado;
    private String strURL = "http://201.20.72.9:8080/ratreamento-war/indicador";

    public DpromConnection(String url) {
        strURL = url;
    }

    public boolean checkNet() throws Exception {
        boolean check = false;
        String myResult = null;
        conectado = false;
//        myResult = sendHttpGet(strURL + "?net=OK");
        if (myResult.startsWith("OK")) {
            check = true;
            conectado = true;
        } else {
            throw new Exception("Problema de conexão com o servidor: " + myResult);
            //check = false;
        }
        return check;
    }

    private String sendHttpPost(String url, byte[] buffer) throws Exception {
        HttpURLConnection hcon = null;
        DataInputStream dis = null;
        DataOutputStream dos = null;
        StringBuffer responseMessage = new StringBuffer();
        // the request body

        try {
            hcon = (HttpURLConnection) Connector.open(url, Connector.READ_WRITE);
            hcon.setRequestProperty("IF-Modified-Since", "30 Jan 2001 16:19:14 GMT");
            hcon.setRequestProperty("User-Agent", "Profile/MIDP-1.0 Confirguration/CLDC-1.0");
            hcon.setRequestProperty("Content-Language", "pt-BR");
            hcon.setRequestProperty("Content-Type", "buffer_medicoes");

            hcon.setRequestMethod("POST");
            hcon.setRequestProperty("Content-Length", Integer.toString(buffer.length));
            dos = (DataOutputStream) hcon.getOutputStream();
            dos.write(buffer);
            dos.close();

            if (hcon.getResponseCode() == HttpURLConnection.HTTP_OK) {
                dis = new DataInputStream(hcon.getInputStream());
                int ch;
                while ((ch = dis.read()) != -1) {
                    responseMessage.append((char) ch);
                }
                dis.close();
            } else {
                throw new Exception(String.valueOf(hcon.getResponseCode()));
            }
        } finally {
            if (hcon != null) {
                hcon.disconnect();
            }
            if (dis != null) {
                dis.close();
            }
            if (dos != null) {
                dos.close();
            }
        }
        return responseMessage.toString();
    }

    private String sendHttpPost(String url, String requeststring) throws Exception {
        HttpURLConnection hcon = null;
        DataInputStream dis = null;
        DataOutputStream dos = null;
        StringBuffer responseMessage = new StringBuffer();
        // the request body

        try {
            hcon = (HttpURLConnection) Connector.open(url, Connector.READ_WRITE);
            hcon.setRequestProperty("IF-Modified-Since", "30 Jan 2001 16:19:14 GMT");
            hcon.setRequestProperty("User-Agent", "Profile/MIDP-1.0 Confirguration/CLDC-1.0");
            hcon.setRequestProperty("Content-Language", "pt-BR");
            hcon.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

            hcon.setRequestMethod("POST");
            byte[] request_body = requeststring.getBytes();
            hcon.setRequestProperty("Content-Length", Integer.toString(request_body.length));
            dos = (DataOutputStream) hcon.getOutputStream();
            dos.write(request_body);
            dos.close();

            if (hcon.getResponseCode() == HttpURLConnection.HTTP_OK) {
                dis = new DataInputStream(hcon.getInputStream());
                int ch;
                while ((ch = dis.read()) != -1) {
                    responseMessage.append((char) ch);
                }
                dis.close();
            } else {
                throw new Exception(String.valueOf(hcon.getResponseCode()));
            }
        } finally {
            if (hcon != null) {
                hcon.disconnect();
            }
            if (dis != null) {
                dis.close();
            }
            if (dos != null) {
                dos.close();
            }
        }
        return responseMessage.toString();
    }

    /*
     * public boolean updateConfig(Configuracao config) throws Exception {
     * JSONObject outer = new JSONObject();
     * 
     * outer.put("imei", System.getProperty("com.nokia.mid.imei"));
     * outer.put("version", config.getVersion());
     * 
     * String strRetornoPC = sendHttpPost(strURL, "configuracao=" + outer.toString());
     * JSONObject inner = new JSONObject(strRetornoPC);
     * if (inner != null) {
     * JSONObject configuracao = inner.getJSONObject("configuracao");
     * if (configuracao != null) {
     * config.setUrlDPROM(configuracao.getString("urlDPROM"));
     * config.setUrlUpdate(configuracao.getString("urlUpdate"));
     * config.setSalvar(configuracao.getBoolean("salvar"));
     * config.setUpdateNeed(configuracao.getBoolean("updateNeed"));
     * String version = configuracao.getString("version");
     * config.setUpdateNeed(!config.getVersion().equals(version));
     * config.setVersion(version);
     * }
     * }
     * 
     * return config.isSalvar();
     * }
     * 
     * public boolean sendIndicadores(Vector indicadores) throws Exception {
     * JSONObject outer = new JSONObject();
     * JSONArray arrayIndicador = new JSONArray();
     * outer.put("imei", System.getProperty("com.nokia.mid.imei"));
     * if (indicadores != null) {
     * for (int i = 0; i < indicadores.size(); i++) {
     * JSONObject ij = new JSONObject();
     * Indicador ind = (Indicador) indicadores.elementAt(i);
     * ij.put("bluetoothAddress", ind.getBluetoothAddress());
     * ij.put("friendlyName", ind.getFriendlyName());
     * ij.put("latitude", ind.getLatitude());
     * ij.put("longitude", ind.getLongitude());
     * ij.put("drpFaseA24", ind.getDrpFaseA24());
     * ij.put("drpFaseA72", ind.getDrpFaseA72());
     * ij.put("drpFaseA168", ind.getDrpFaseA168());
     * ij.put("drpFaseB24", ind.getDrpFaseB24());
     * ij.put("drpFaseB72", ind.getDrpFaseB72());
     * ij.put("drpFaseB168", ind.getDrpFaseB168());
     * ij.put("drpFaseC24", ind.getDrpFaseC24());
     * ij.put("drpFaseC72", ind.getDrpFaseC72());
     * ij.put("drpFaseC168", ind.getDrpFaseC168());
     * ij.put("drcFaseA24", ind.getDrcFaseA24());
     * ij.put("drcFaseA72", ind.getDrcFaseA72());
     * ij.put("drcFaseA168", ind.getDrcFaseA168());
     * ij.put("drcFaseB24", ind.getDrcFaseB24());
     * ij.put("drcFaseB72", ind.getDrcFaseB72());
     * ij.put("drcFaseB168", ind.getDrcFaseB168());
     * ij.put("drcFaseC24", ind.getDrcFaseC24());
     * ij.put("drcFaseC72", ind.getDrcFaseC72());
     * ij.put("drcFaseC168", ind.getDrcFaseC168());
     * arrayIndicador.put(ij);
     * }
     * outer.put("indicadores", arrayIndicador);
     * }
     * String strRetornoPC = sendHttpPost(strURL, "indicador=" + outer.toString());
     * if (strRetornoPC.equals("{true}")) {
     * return true;
     * } else {
     * throw new Exception("Erro no servidor: " + strRetornoPC);
     * }
     * }
     * 
     * public boolean sendMedicoes(Vector medicoes) throws Exception {
     * JSONObject outer = new JSONObject();
     * JSONArray arrayMedicoes = new JSONArray();
     * 
     * outer.put("imei", System.getProperty("com.nokia.mid.imei"));
     * if (medicoes != null) {
     * for (int i = 0; i < medicoes.size(); i++) {
     * JSONObject ij = new JSONObject();
     * MedicaoModel ins = (MedicaoModel) medicoes.elementAt(i);
     * ij.put("indicador", ins.getIndicador());
     * ij.put("dataMedicao", ins.getDataMedicao().getTime());
     * ij.put("tensaoA", ins.getTensaoA());
     * ij.put("tensaoB", ins.getTensaoB());
     * ij.put("tensaoC", ins.getTensaoC());
     * ij.put("tensaoAmin", ins.getTensaoAmin());
     * ij.put("tensaoBmin", ins.getTensaoBmin());
     * ij.put("tensaoCmin", ins.getTensaoCmin());
     * ij.put("tensaoAmax", ins.getTensaoAmax());
     * ij.put("tensaoBmax", ins.getTensaoBmax());
     * ij.put("tensaoCmax", ins.getTensaoCmax());
     * arrayMedicoes.put(ij);
     * }
     * outer.put("medicoes", arrayMedicoes);
     * }
     * String strRetornoPC = sendHttpPost(strURL, "medicoes=" + outer.toString());
     * if (strRetornoPC.equals("{true}")) {
     * return true;
     * } else {
     * throw new Exception("Erro no servidor: " + strRetornoPC);
     * }
     * }
     */

    public boolean sendMedicoes(byte[] buffer) throws Exception {
        String strRetornoPC = sendHttpPost(strURL, buffer);
        if (strRetornoPC.equals("{true}")) {
            return true;
        } else {
            throw new Exception("Erro no servidor: " + strRetornoPC);
        }
    }
}
