/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.coelce.indicador.model;

import javax.persistence.Entity;
import javax.persistence.Transient;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 *
 * @author Paulo Roberto
 */
@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Indicador extends BaseModel {

    private String friendlyName;
    private String bluetoothAddress;
    @Transient
    private String estado = "conectar";

    public Indicador() {
    }

    public Indicador(String friendlyName, String bluetoothAddress) {
        this.friendlyName = friendlyName;
        this.bluetoothAddress = bluetoothAddress;
    }

    public String getBluetoothAddress() {
        return bluetoothAddress;
    }

    public void setBluetoothAddress(String bluetoothAddress) {
        this.bluetoothAddress = bluetoothAddress;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    public void setFriendlyName(String friendlyName) {
        this.friendlyName = friendlyName;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
