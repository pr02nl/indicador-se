/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.coelce.indicador.business;

import br.gov.frameworkdemoiselle.stereotype.BusinessController;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import java.util.ArrayList;
import java.util.Enumeration;
import net.wimpi.modbus.Modbus;
import net.wimpi.modbus.io.ModbusSerialTransaction;
import net.wimpi.modbus.io.ModbusTransaction;
import net.wimpi.modbus.net.Connection;
import net.wimpi.modbus.net.SerialConnection;
import net.wimpi.modbus.util.SerialParameters;

/**
 *
 * @author pr02nl
 */
@BusinessController
public class ClienteSerial extends ClienteIndicador {

    SerialConnection connection = null;
    ModbusSerialTransaction transaction = null;
    private ArrayList<String> portas;

    @Override
    public Connection getConnection() {
        return connection;
    }

    @Override
    public void setConnection(Connection con) {
        connection = (SerialConnection) con;
    }

    @Override
    public ModbusTransaction getTransaction() {
        return transaction;
    }

    @Override
    public void setTransaction(ModbusTransaction trans) {
        transaction = (ModbusSerialTransaction) trans;
    }

    public ArrayList<String> getPortas() {
        if (portas == null) {
            Enumeration listaDePortas = CommPortIdentifier.getPortIdentifiers();
            portas = new ArrayList<String>();
            while (listaDePortas.hasMoreElements()) {
                CommPortIdentifier ips = (CommPortIdentifier) listaDePortas.nextElement();
                if (ips.getPortType() == CommPortIdentifier.PORT_SERIAL) {
                    portas.add(ips.getName());
                }
            }
        }
        return portas;
    }

    @Override
    public int connect(int index) throws Exception {
        if (getConnection() == null || !getConnection().isOpen()) {
            SerialParameters params = new SerialParameters();
            params.setPortName(getPortas().get(index));
            params.setBaudRate(115200);
            params.setDatabits(8);
            params.setParity(SerialPort.PARITY_NONE);
            params.setStopbits(1);
            params.setEncoding(Modbus.SERIAL_ENCODING_ASCII);
            params.setEcho(false);
            setConnection(new SerialConnection(params));
            getConnection().open();
            setTransaction(new ModbusSerialTransaction((SerialConnection) getConnection()));
        }
        return firstConect();
    }
}
