/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.coelce.indicador.config;

import br.gov.frameworkdemoiselle.configuration.Configuration;

/**
 *
 * @author Paulo Roberto
 */
@Configuration(resource = "config")
public class ViewConfig {

    private int viewBlockSize;
    private String server;

    public int getViewBlockSize() {
        return viewBlockSize;
    }

    public String getServer() {
        return server;
    }
}
