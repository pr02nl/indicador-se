package br.com.coelce.indicador;

import br.com.coelce.indicador.frame.IndicadorApp;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.swing.SwingUtilities;
import org.jboss.weld.environment.se.StartMain;
import org.jboss.weld.environment.se.events.ContainerInitialized;
import org.openswing.swing.mdi.client.MDIFrame;

public class Main {

    @Inject
    private IndicadorApp indicadorApp;

    public void start(@Observes ContainerInitialized event) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new MDIFrame(indicadorApp);
            }
        });
    }

    public static void main(String[] args) {
        new StartMain(args).go();
    }
}
