/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.coelce.indicador.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 *
 * @author Paulo
 */
@Entity
public class CentroServico extends BaseModel {

    @ManyToOne
    private Departamento departamento;
    private String sigla;
    private String nome;

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public String toString() {
        return getSigla();
    }
}
