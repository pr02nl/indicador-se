/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.coelce.indicador.persistence;

import br.com.coelce.indicador.model.Aquisicao;
import br.com.coelce.indicador.model.Medidas;
import br.com.pr02nl.model.persistence.DaoGenerico;
import br.gov.frameworkdemoiselle.stereotype.PersistenceController;
import br.gov.frameworkdemoiselle.transaction.Transactional;
import javax.persistence.Query;

/**
 *
 * @author Paulo Roberto
 */
@PersistenceController
public class MedidasDAO extends DaoGenerico<Medidas, Long> {

    @Transactional
    public int deleteAllByAquisicao(Aquisicao aquisicao) {
        Query createQuery = getEntityManager().createQuery("delete from " + getBeanClass().getSimpleName() + " this where this.aquisicao = :aquisicao");
        return createQuery.setParameter("aquisicao", aquisicao).executeUpdate();
    }
}
