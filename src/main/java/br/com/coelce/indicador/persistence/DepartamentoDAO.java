/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.coelce.indicador.persistence;

import br.com.coelce.indicador.model.Departamento;
import br.com.pr02nl.model.persistence.DaoGenerico;
import br.gov.frameworkdemoiselle.stereotype.PersistenceController;

/**
 *
 * @author pr02nl
 */
@PersistenceController
public class DepartamentoDAO extends DaoGenerico<Departamento, Long> {
}
