/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.coelce.indicador.persistence;

import br.com.coelce.indicador.business.CentroServicoBC;
import br.com.coelce.indicador.business.DepartamentoBC;
import br.com.coelce.indicador.config.ViewConfig;
import br.com.coelce.indicador.model.CentroServico;
import br.com.coelce.indicador.model.Departamento;
import br.com.coelce.indicador.security.ConfigInstance;
//import br.gov.frameworkdemoiselle.annotation.Shutdown;
//import br.gov.frameworkdemoiselle.annotation.Startup;
import br.gov.frameworkdemoiselle.util.ResourceBundle;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import java.io.IOException;
import java.util.List;
import javax.inject.Inject;
import javax.swing.JOptionPane;
import javax.ws.rs.core.MediaType;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.openswing.swing.client.OptionPane;
import org.slf4j.Logger;

/**
 *
 * @author Paulo Roberto
 */
//@ApplicationScoped
public class DatabaseServer {

//    private final Server server;
    @Inject
    private Logger logger;
    @Inject
    private ResourceBundle bundle;
    @Inject
    private DepartamentoBC departamentoBC;
    @Inject
    private CentroServicoBC centroServicoBC;
    @Inject
    private ConfigInstance configInstance;
    @Inject
    private ViewConfig viewConfig;

    public DatabaseServer() {
//        this.server = new Server();
//        server.setDatabaseName(0, "indicador");
//        server.setDatabasePath(0, "database/indicador");
//        //server.setDatabaseName(1, "audit");
//        //server.setDatabasePath(1, "database/audit");
//        server.setPort(9001);
//        server.setSilent(true);
    }

//    @Startup(priority = Startup.MAX_PRIORITY)
    public void start() {
//        try {
//            //        this.server.start();
//            testaServers();
//        } catch (Exception ex) {
//            this.logger.error(null, ex);
//            return;
//        }
        List<Departamento> findAll = departamentoBC.findAll();
        if (findAll.isEmpty()) {
            getDepartamentos();
        } else {
            configInstance.setDepartamento(findAll.get(0));
        }
        List<CentroServico> findAll1 = centroServicoBC.findAll();
        if (findAll1.isEmpty()) {
            getCentroServico(configInstance.getDepartamento());
        } else {
            configInstance.setCentroServico(findAll1.get(0));
        }
        this.logger.info(bundle.getString("database.loaded"));
    }

//    @Shutdown
    public void stop() {
//        this.server.shutdown();
        this.logger.info(bundle.getString("database.stopped"));
    }

    private void getCentroServico(Departamento departamento) {
        ClientConfig clienteConfig = new DefaultClientConfig();
        clienteConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
        Client client = Client.create(clienteConfig);
        String json = client.resource(String.format("http://%s:8080", viewConfig.getServer())).
                path("planejamento").
                path("sincronizar").
                path("centroServicoService").
                path("getCentroServicos").
                path(departamento.getNome()).
                accept(MediaType.APPLICATION_JSON).
                get(String.class);
        ObjectMapper mapper = new ObjectMapper();
        Object readValue;
        try {
            readValue = mapper.readValue(json, new TypeReference<List<CentroServico>>() {
            });
            if (readValue instanceof List) {
                List centroServicos = (List) readValue;
                Object result = OptionPane.showInputDialog(null, "configuracao.centro.servicos", "configuracao", JOptionPane.QUESTION_MESSAGE, null, centroServicos.toArray(), null);
                if (result instanceof CentroServico) {
                    CentroServico c = (CentroServico) result;
                    c.setId(null);
                    c.setDepartamento(configInstance.getDepartamento());
                    centroServicoBC.insert((CentroServico) result);
                    configInstance.setCentroServico(c);
                }
            }
        } catch (IOException ex) {
            this.logger.error(json, ex);
        }
    }

    private void getDepartamentos() {
        ClientConfig clienteConfig = new DefaultClientConfig();
        clienteConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
        Client client = Client.create(clienteConfig);
        String json = client.resource(String.format("http://%s:8080", viewConfig.getServer())).
                path("planejamento").
                path("sincronizar").
                path("departamentoService").
                path("getDepartamentos").
                accept(MediaType.APPLICATION_JSON).
                get(String.class);
        ObjectMapper mapper = new ObjectMapper();
        Object readValue;
        try {
            readValue = mapper.readValue(json, new TypeReference<List<Departamento>>() {
            });
            if (readValue instanceof List) {
                List departamentos = (List) readValue;
//                for (Iterator it = departamentos.iterator(); it.hasNext();) {
//                    Departamento object = (Departamento) it.next();
//                    departamentoBC.insert(object);
//                }
                Object result = OptionPane.showInputDialog(null, "configuracao.departamento", "configuracao", JOptionPane.QUESTION_MESSAGE, null, departamentos.toArray(), null);
                if (result instanceof Departamento) {
                    Departamento d = (Departamento) result;
                    d.setId(null);
                    departamentoBC.insert(d);
                    configInstance.setDepartamento(d);
                }
            }
//            logger.info(readValue.toString());
            //        ClientResponse get = resource.get(ClientResponse.class);
            //        if (get.getClientResponseStatus() == ClientResponse.Status.OK) {
            //        }
            //        }
        } catch (IOException ex) {
            this.logger.error(json, ex);
        }
    }

//    private void testaServers() throws Exception {
//        ClientConfig clienteConfig = new DefaultClientConfig();
//        Client client = Client.create(clienteConfig);
//        client.setReadTimeout(5000);
//        WebResource resource = client.resource(String.format("http://%s:8080", viewConfig.getServerRedeInterna()));
//        logger.info(String.format("Tentando conexão com o servidor em %s", viewConfig.getServerRedeInterna()));
//        ClientResponse get = resource.get(ClientResponse.class);
//        if (get.getClientResponseStatus() == ClientResponse.Status.OK) {
//            configInstance.setServidor(viewConfig.getServerRedeInterna());
//            logger.info(String.format("Conexão com o servidor %s bem sucedida", viewConfig.getServerRedeInterna()));
//            return;
//        }
//        resource = client.resource(String.format("http://%s:8080", viewConfig.getServerRedeCoelce()));
//        logger.info(String.format("Tentando conexão com o servidor em %s", viewConfig.getServerRedeCoelce()));
//        get = resource.get(ClientResponse.class);
//        if (get.getClientResponseStatus() == ClientResponse.Status.OK) {
//            configInstance.setServidor(viewConfig.getServerRedeCoelce());
//            logger.info(String.format("Conexão com o servidor %s bem sucedida", viewConfig.getServerRedeCoelce()));
//            return;
//        }
//        resource = client.resource(String.format("http://%s:8080", viewConfig.getServerRedeExterna()));
//        logger.info(String.format("Tentando conexão com o servidor em %s", viewConfig.getServerRedeExterna()));
//        get = resource.get(ClientResponse.class);
//        if (get.getClientResponseStatus() == ClientResponse.Status.OK) {
//            configInstance.setServidor(viewConfig.getServerRedeExterna());
//            logger.info(String.format("Conexão com o servidor %s bem sucedida", viewConfig.getServerRedeExterna()));
//            return;
//        }
//        logger.error("Não foi possivel encontrar o servidor em nenhum dos endereçoes conhecidos.");
//    }
}
