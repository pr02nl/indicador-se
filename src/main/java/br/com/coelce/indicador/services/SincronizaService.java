/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.coelce.indicador.services;

import br.com.coelce.indicador.business.AquisicaoBC;
import br.com.coelce.indicador.business.IndicadorBC;
import br.com.coelce.indicador.business.MedidasBC;
import br.com.coelce.indicador.config.ViewConfig;
import br.com.coelce.indicador.model.Aquisicao;
import br.com.coelce.indicador.model.Indicador;
import br.com.coelce.indicador.model.Medidas;
import br.com.coelce.indicador.security.ConfigInstance;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.core.MediaType;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.slf4j.Logger;

/**
 *
 * @author pr02nl
 */
@Singleton
public class SincronizaService {

    @Inject
    private ConfigInstance configInstance;
    @Inject
    private ViewConfig viewConfig;
    @Inject
    private Logger logger;
    @Inject
    private IndicadorBC indicadorBC;
    @Inject
    private AquisicaoBC aquisicaoBC;
    @Inject
    private MedidasBC medidasBC;
    private WebResource resource;

    @PostConstruct
    public void init() {
        ClientConfig clienteConfig = new DefaultClientConfig();
        clienteConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
        Client client = Client.create(clienteConfig);
        resource = client.resource(String.format("http://%s:8080", viewConfig.getServer())).
                path("planejamento").
                path("sincronizar");
    }

    public void sincronizar() {
        sincronizaIndicadores();
        List<Indicador> findAll = indicadorBC.findAll();
        for (Indicador indicador : findAll) {
            sincronizaAquisicoes(indicador);
        }
        List<Aquisicao> findAll1 = aquisicaoBC.findAll();
        for (Aquisicao aquisicao : findAll1) {
            sincronizaMedicoes(aquisicao);
        }
    }

    public void sincronizaMedicoes(Aquisicao aquisicao) {
        Integer countServer = resource.
                path("indicadorService").
                path("countMedicoes").
                path(aquisicao.getIndicador().getBluetoothAddress()).
                path(Long.toString(aquisicao.getDataAquisicao().getTime())).
                accept(MediaType.APPLICATION_JSON).
                get(Integer.class);
        int countLocal = medidasBC.countByAquisicao(aquisicao);
        if (countServer == 0 || countLocal == countServer) {
            return;
        }
        medidasBC.deleteAllByAquisicao(aquisicao);
        String json = resource.
                path("indicadorService").
                path("getMedicoes").
                path(aquisicao.getIndicador().getBluetoothAddress()).
                path(Long.toString(aquisicao.getDataAquisicao().getTime())).
                accept(MediaType.APPLICATION_JSON).
                get(String.class);
        ObjectMapper mapper = new ObjectMapper();
        Object readValue = null;
        try {
            readValue = mapper.readValue(json, new TypeReference<List<Medidas>>() {
            });
        } catch (IOException ex) {
            this.logger.error(json, ex);
        }
        if (readValue instanceof List) {
            List indicadores = (List) readValue;
            for (Iterator it = indicadores.iterator(); it.hasNext();) {
                Medidas medida = (Medidas) it.next();
                medida.setId(null);
                medida.setAquisicao(aquisicao);
                medidasBC.insert(medida);
            }
        }
        Integer countExclidos = resource.
                path("indicadorService").
                path("deleteMedicoes").
                path(aquisicao.getIndicador().getBluetoothAddress()).
                path(Long.toString(aquisicao.getDataAquisicao().getTime())).
                accept(MediaType.APPLICATION_JSON).
                get(Integer.class);
        countLocal = medidasBC.countByAquisicao(aquisicao);
        if (countExclidos != countLocal) {
            this.logger.error("Numero de registros diferentes, excluidos e salvos");
        }
    }

    public void sincronizaAquisicoes(Indicador indicador) {
        Integer countServer = resource.
                path("indicadorService").
                path("countAquisicoes").
                path(indicador.getBluetoothAddress()).
                accept(MediaType.APPLICATION_JSON).
                get(Integer.class);
        int countLocal = aquisicaoBC.countByIndicador(indicador);
        if (countServer == 0 || countLocal == countServer) {
            return;
        }
        String json = resource.
                path("indicadorService").
                path("getAquisicoes").
                path(indicador.getBluetoothAddress()).
                accept(MediaType.APPLICATION_JSON).
                get(String.class);
        ObjectMapper mapper = new ObjectMapper();
        Object readValue = null;
        try {
            readValue = mapper.readValue(json, new TypeReference<List<Aquisicao>>() {
            });
        } catch (IOException ex) {
            this.logger.error(json, ex);
        }
        if (readValue instanceof List) {
            List aquisicoes = (List) readValue;
            for (Iterator it = aquisicoes.iterator(); it.hasNext();) {
                Aquisicao aquisicao = (Aquisicao) it.next();
                Aquisicao aquisicaoLocal = aquisicaoBC.findByIndicadorAndDataAquisicao(indicador, aquisicao.getDataAquisicao());
                if (aquisicaoLocal == null) {
                    aquisicao.setIndicador(indicador);
                    aquisicao.setId(null);
                    aquisicaoBC.insert(aquisicao);
                } else {
                    aquisicaoLocal.setDataEnvio(aquisicao.getDataEnvio());
                    aquisicaoLocal.setDrcFaseA168(aquisicao.getDrcFaseA168());
                    aquisicaoLocal.setDrcFaseA24(aquisicao.getDrcFaseA24());
                    aquisicaoLocal.setDrcFaseA72(aquisicao.getDrcFaseA72());
                    aquisicaoLocal.setDrcFaseB168(aquisicao.getDrcFaseB168());
                    aquisicaoLocal.setDrcFaseB24(aquisicao.getDrcFaseB24());
                    aquisicaoLocal.setDrcFaseB72(aquisicao.getDrcFaseB72());
                    aquisicaoLocal.setDrcFaseC168(aquisicao.getDrcFaseC168());
                    aquisicaoLocal.setDrcFaseC24(aquisicao.getDrcFaseC24());
                    aquisicaoLocal.setDrcFaseC72(aquisicao.getDrcFaseC72());
                    aquisicaoLocal.setDrpFaseA168(aquisicao.getDrpFaseA168());
                    aquisicaoLocal.setDrpFaseA24(aquisicao.getDrpFaseA24());
                    aquisicaoLocal.setDrpFaseA72(aquisicao.getDrpFaseA72());
                    aquisicaoLocal.setDrpFaseB168(aquisicao.getDrpFaseB168());
                    aquisicaoLocal.setDrpFaseB24(aquisicao.getDrpFaseB24());
                    aquisicaoLocal.setDrpFaseB72(aquisicao.getDrpFaseB72());
                    aquisicaoLocal.setDrpFaseC168(aquisicao.getDrpFaseC168());
                    aquisicaoLocal.setDrpFaseC24(aquisicao.getDrpFaseC24());
                    aquisicaoLocal.setDrpFaseC72(aquisicao.getDrpFaseC72());
                    aquisicaoBC.update(aquisicaoLocal);
                }
            }
        }
    }

    public void sincronizaIndicadores() {
        Integer countServer = resource.
                path("indicadorService").
                path("countIndicadores").
                path(configInstance.getCentroServico().getSigla()).
                accept(MediaType.APPLICATION_JSON).
                get(Integer.class);
        int countLocal = indicadorBC.count();
        if (countServer == 0 || countLocal == countServer) {
            return;
        }
        String json = resource.
                path("indicadorService").
                path("getIndicadores").
                path(configInstance.getCentroServico().getSigla()).
                accept(MediaType.APPLICATION_JSON).
                get(String.class);
        ObjectMapper mapper = new ObjectMapper();
        Object readValue = null;
        try {
            readValue = mapper.readValue(json, new TypeReference<List<Indicador>>() {
            });
        } catch (IOException ex) {
            this.logger.error(json, ex);
        }
        if (readValue instanceof List) {
            List indicadores = (List) readValue;
            for (Iterator it = indicadores.iterator(); it.hasNext();) {
                Indicador indicador = (Indicador) it.next();
                String bluetoothAddress = indicador.getBluetoothAddress();
                if (bluetoothAddress != null && !"".equals(bluetoothAddress)) {
                    Indicador findByBluetoothAddress = indicadorBC.findByBluetoothAddress(bluetoothAddress);
                    if (findByBluetoothAddress == null) {
                        indicador.setId(null);
                        indicadorBC.insert(indicador);
                    } else {
                        findByBluetoothAddress.setFriendlyName(indicador.getFriendlyName());
                        indicadorBC.update(findByBluetoothAddress);
                    }
                }
            }
        }
    }
}
