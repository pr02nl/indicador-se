/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.coelce.indicador.business;

import br.com.coelce.indicador.model.Aquisicao;
import br.com.coelce.indicador.model.Indicador;
import br.com.coelce.indicador.persistence.AquisicaoDAO;
import br.com.pr02nl.business.MyDelegateCrud;
import br.gov.frameworkdemoiselle.stereotype.BusinessController;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Paulo Roberto
 */
@BusinessController
public class AquisicaoBC extends MyDelegateCrud<Aquisicao, Long, AquisicaoDAO> {

    public int countByIndicador(Indicador indicador) {
        Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("indicador", indicador);
        return count(null, parametros);
    }

    public Aquisicao findByIndicadorAndDataAquisicao(Indicador indicador, Date dataAquisicao) {
        Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("indicador", indicador);
        parametros.put("dataAquisicao", dataAquisicao);
        List<Aquisicao> findAll = findAll(null, parametros, null, null);
        if (!findAll.isEmpty()) {
            return findAll.get(0);
        }
        return null;
    }
}
