/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.coelce.indicador.view;

import br.com.coelce.indicador.business.AquisicaoBC;
import br.com.coelce.indicador.config.ViewConfig;
import br.com.coelce.indicador.exceptions.ValidationException;
import br.com.coelce.indicador.model.Aquisicao;
import br.gov.frameworkdemoiselle.exception.ExceptionHandler;
import br.gov.frameworkdemoiselle.stereotype.ViewController;
import java.util.ArrayList;
import java.util.Map;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOListResponse;
import org.openswing.swing.message.receive.java.VOResponse;
import org.openswing.swing.message.send.java.GridParams;
import org.openswing.swing.table.client.GridController;
import org.openswing.swing.table.java.GridDataLocator;

/**
 *
 * @author Paulo Roberto
 */
@ViewController
@Singleton
public class AquisicaoView extends GridController implements GridDataLocator {

    @Inject
    private AquisicaoBC aquisicaoBC;
    @Inject
    private ViewConfig config;
    private Aquisicao currentAquisicao;

    public Response loadData(int action, int startIndex, Map filteredColumns,
            ArrayList currentSortedColumns, ArrayList currentSortedVersusColumns,
            Class valueObjectType, Map otherGridParams) {
        int count = aquisicaoBC.count(filteredColumns, otherGridParams);
        if (action == GridParams.PREVIOUS_BLOCK_ACTION) {
            action = GridParams.NEXT_BLOCK_ACTION;
            startIndex = Math.max(startIndex - config.getViewBlockSize(), 0);
        }
        return new VOListResponse(aquisicaoBC.findAll(filteredColumns, otherGridParams,
                new int[]{startIndex, config.getViewBlockSize()}, currentSortedColumns,
                currentSortedVersusColumns), count > startIndex + config.getViewBlockSize(), aquisicaoBC.findAll().size());
    }

    @Override
    public Response insertRecords(int[] rowNumbers, ArrayList newValueObjects) throws Exception {
        for (Object object : newValueObjects) {
            Aquisicao entity = (Aquisicao) object;
            aquisicaoBC.insert(entity);
        }
        return new VOListResponse(newValueObjects, false, newValueObjects.size());
    }

    @Override
    public Response updateRecords(int[] rowNumbers, ArrayList oldPersistentObjects, ArrayList persistentObjects) throws Exception {
        for (Object object : persistentObjects) {
            Aquisicao entity = (Aquisicao) object;
            aquisicaoBC.update(entity);
        }
        return new VOListResponse(persistentObjects, false, persistentObjects.size());
    }

    @Override
    public Response deleteRecords(ArrayList persistentObjects) throws Exception {
        for (Object object : persistentObjects) {
            Aquisicao entity = (Aquisicao) object;
            aquisicaoBC.delete(entity.getId());
        }
        return new VOResponse(true);
    }

    public Aquisicao getCurrentAquisicao() {
        return currentAquisicao;
    }

    public void setCurrentAquisicao(Aquisicao currentAquisicao) {
        this.currentAquisicao = currentAquisicao;
    }

    @ExceptionHandler
    public void handleExceptions(ConstraintViolationException exception) {
        String errorMessage = "";
        for (ConstraintViolation<?> c : exception.getConstraintViolations()) {
            String msg = c.getMessage();
            if (msg.startsWith("{")) {
                msg = msg.substring(1, msg.lastIndexOf("}"));
            }
            //msg = Bundle.getString(msg);
            errorMessage += msg + "\n";
        }
        throw new ValidationException(errorMessage);
    }
}
