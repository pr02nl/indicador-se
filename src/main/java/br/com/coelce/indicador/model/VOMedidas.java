/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.coelce.indicador.model;

import java.util.Date;

/**
 *
 * @author Paulo Roberto
 */
public class VOMedidas extends BaseModel {

    private Date dataMedicao;
    private double tensaoA;
    private double tensaoB;
    private double tensaoC;
    private int numRegistros;
    private boolean medindo;

    public Date getDataMedicao() {
        return dataMedicao;
    }

    public void setDataMedicao(Date dataMedicao) {
        this.dataMedicao = dataMedicao;
    }

    public double getTensaoA() {
        return tensaoA;
    }

    public void setTensaoA(double tensaoA) {
        this.tensaoA = tensaoA;
    }

    public double getTensaoB() {
        return tensaoB;
    }

    public void setTensaoB(double tensaoB) {
        this.tensaoB = tensaoB;
    }

    public double getTensaoC() {
        return tensaoC;
    }

    public void setTensaoC(double tensaoC) {
        this.tensaoC = tensaoC;
    }

    public int getNumRegistros() {
        return numRegistros;
    }

    public void setNumRegistros(int numRegistros) {
        this.numRegistros = numRegistros;
    }

    public boolean isMedindo() {
        return medindo;
    }

    public void setMedindo(boolean medindo) {
        this.medindo = medindo;
    }
}
