/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.coelce.indicador.view;

import br.com.coelce.indicador.business.ClientBluetooth;
import br.com.coelce.indicador.model.Indicador;
import br.gov.frameworkdemoiselle.stereotype.ViewController;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.inject.Singleton;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOListResponse;
import org.openswing.swing.table.client.GridController;
import org.openswing.swing.table.java.GridDataLocator;

/**
 *
 * @author Paulo Roberto
 */
@ViewController
@Singleton
public class BluetoothDataLocator extends GridController implements GridDataLocator {

    @Inject
    private ClientBluetooth bluetooth;
    @Inject
    private org.slf4j.Logger logger;

    @Override
    public Response loadData(int action, int startIndex, Map filteredColumns, ArrayList currentSortedColumns, ArrayList currentSortedVersusColumns, Class valueObjectType, Map otherGridParams) {
        String error = "";
        try {
            List<Indicador> dispositivos = getBluetooth().getDispositivos();
            return new VOListResponse(dispositivos, false, dispositivos.size());
        } catch (IOException ex) {
            logger.error("", ex);
            error = ex.getLocalizedMessage();
        } catch (InterruptedException ex) {
            logger.error("", ex);
            error = ex.getLocalizedMessage();
        }
        return new ErrorResponse(error);
    }

    @Override
    public Response updateRecords(int[] rowNumbers, ArrayList oldPersistentObjects, ArrayList persistentObjects) throws Exception {
        for (Iterator it = persistentObjects.iterator(); it.hasNext();) {
            Indicador e = (Indicador) it.next();
            e.setEstado("conectar");
        }
        return new VOListResponse(persistentObjects, false, persistentObjects.size());
    }

    @Override
    public void afterReloadGrid() {
        try {
            getBluetooth().disconnect();
        } catch (IOException ex) {
            Logger.getLogger(BluetoothDataLocator.class.getName()).log(Level.SEVERE, null, ex);
        }
        super.afterReloadGrid();
    }

    /**
     * @return the bluetooth
     */
    public ClientBluetooth getBluetooth() {
        return bluetooth;
    }

    /**
     * @param bluetooth the bluetooth to set
     */
    public void setBluetooth(ClientBluetooth bluetooth) {
        this.bluetooth = bluetooth;
    }
}
