/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.coelce.indicador.business;

import br.gov.frameworkdemoiselle.stereotype.BusinessController;
import java.io.*;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author Paulo Roberto
 */
@BusinessController
@Singleton
public class UpdateFirmware implements Runnable {

    @Inject
    private org.slf4j.Logger logger;
//    @Inject
//    private ClientBluetooth bluetooth;
    
    @Override
    public void run() {
        JFileChooser fc = new JFileChooser();
        fc.setFileFilter(new FileNameExtensionFilter("Intel HEX File", "hex"));
        fc.setAcceptAllFileFilterUsed(false);
        if (fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            try {
                BufferedReader leitor = new BufferedReader(new FileReader(file));
                String line;
                while ((line = leitor.readLine()) != null) {
                    
                }
            } catch (IOException ex) {
                logger.error(null, ex);
            }
        }
    }
}
