/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.coelce.indicador.model;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

/**
 *
 * @author Paulo Roberto
 */
@Entity
public class Aquisicao extends BaseModel {

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dataAquisicao;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dataEnvio;
    private double latitude;
    private double longitude;
    private double drpFaseA24;
    private double drpFaseA72;
    private double drpFaseA168;
    private double drpFaseB24;
    private double drpFaseB72;
    private double drpFaseB168;
    private double drpFaseC24;
    private double drpFaseC72;
    private double drpFaseC168;
    private double drcFaseA24;
    private double drcFaseA72;
    private double drcFaseA168;
    private double drcFaseB24;
    private double drcFaseB72;
    private double drcFaseB168;
    private double drcFaseC24;
    private double drcFaseC72;
    private double drcFaseC168;
    @ManyToOne
    private Indicador indicador;

    public Date getDataAquisicao() {
        return dataAquisicao;
    }

    public void setDataAquisicao(Date dataAquisicao) {
        this.dataAquisicao = dataAquisicao;
    }

    public double getDrcFaseA168() {
        return drcFaseA168;
    }

    public void setDrcFaseA168(double drcFaseA168) {
        this.drcFaseA168 = drcFaseA168;
    }

    public double getDrcFaseA24() {
        return drcFaseA24;
    }

    public void setDrcFaseA24(double drcFaseA24) {
        this.drcFaseA24 = drcFaseA24;
    }

    public double getDrcFaseA72() {
        return drcFaseA72;
    }

    public void setDrcFaseA72(double drcFaseA72) {
        this.drcFaseA72 = drcFaseA72;
    }

    public double getDrcFaseB168() {
        return drcFaseB168;
    }

    public void setDrcFaseB168(double drcFaseB168) {
        this.drcFaseB168 = drcFaseB168;
    }

    public double getDrcFaseB24() {
        return drcFaseB24;
    }

    public void setDrcFaseB24(double drcFaseB24) {
        this.drcFaseB24 = drcFaseB24;
    }

    public double getDrcFaseB72() {
        return drcFaseB72;
    }

    public void setDrcFaseB72(double drcFaseB72) {
        this.drcFaseB72 = drcFaseB72;
    }

    public double getDrcFaseC168() {
        return drcFaseC168;
    }

    public void setDrcFaseC168(double drcFaseC168) {
        this.drcFaseC168 = drcFaseC168;
    }

    public double getDrcFaseC24() {
        return drcFaseC24;
    }

    public void setDrcFaseC24(double drcFaseC24) {
        this.drcFaseC24 = drcFaseC24;
    }

    public double getDrcFaseC72() {
        return drcFaseC72;
    }

    public void setDrcFaseC72(double drcFaseC72) {
        this.drcFaseC72 = drcFaseC72;
    }

    public double getDrpFaseA168() {
        return drpFaseA168;
    }

    public void setDrpFaseA168(double drpFaseA168) {
        this.drpFaseA168 = drpFaseA168;
    }

    public double getDrpFaseA24() {
        return drpFaseA24;
    }

    public void setDrpFaseA24(double drpFaseA24) {
        this.drpFaseA24 = drpFaseA24;
    }

    public double getDrpFaseA72() {
        return drpFaseA72;
    }

    public void setDrpFaseA72(double drpFaseA72) {
        this.drpFaseA72 = drpFaseA72;
    }

    public double getDrpFaseB168() {
        return drpFaseB168;
    }

    public void setDrpFaseB168(double drpFaseB168) {
        this.drpFaseB168 = drpFaseB168;
    }

    public double getDrpFaseB24() {
        return drpFaseB24;
    }

    public void setDrpFaseB24(double drpFaseB24) {
        this.drpFaseB24 = drpFaseB24;
    }

    public double getDrpFaseB72() {
        return drpFaseB72;
    }

    public void setDrpFaseB72(double drpFaseB72) {
        this.drpFaseB72 = drpFaseB72;
    }

    public double getDrpFaseC168() {
        return drpFaseC168;
    }

    public void setDrpFaseC168(double drpFaseC168) {
        this.drpFaseC168 = drpFaseC168;
    }

    public double getDrpFaseC24() {
        return drpFaseC24;
    }

    public void setDrpFaseC24(double drpFaseC24) {
        this.drpFaseC24 = drpFaseC24;
    }

    public double getDrpFaseC72() {
        return drpFaseC72;
    }

    public void setDrpFaseC72(double drpFaseC72) {
        this.drpFaseC72 = drpFaseC72;
    }

    public Indicador getIndicador() {
        return indicador;
    }

    public void setIndicador(Indicador indicador) {
        this.indicador = indicador;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public Date getDataEnvio() {
        return dataEnvio;
    }

    public void setDataEnvio(Date dataEnvio) {
        this.dataEnvio = dataEnvio;
    }
}
