/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.coelce.indicador.model;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

/**
 *
 * @author Paulo Roberto
 */
@Entity
public class Medidas extends BaseModel {

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dataMedicao;
    private double tensaoA;
    private double tensaoB;
    private double tensaoC;
    private double tensaoAmin;
    private double tensaoBmin;
    private double tensaoCmin;
    private double tensaoAmax;
    private double tensaoBmax;
    private double tensaoCmax;
    @ManyToOne
    private Aquisicao aquisicao;

    public Aquisicao getAquisicao() {
        return aquisicao;
    }

    public void setAquisicao(Aquisicao aquisicao) {
        this.aquisicao = aquisicao;
    }

    public Date getDataMedicao() {
        return dataMedicao;
    }

    public void setDataMedicao(Date dataMedicao) {
        this.dataMedicao = dataMedicao;
    }

    public double getTensaoA() {
        return tensaoA;
    }

    public void setTensaoA(double tensaoA) {
        this.tensaoA = tensaoA;
    }

    public double getTensaoAmax() {
        return tensaoAmax;
    }

    public void setTensaoAmax(double tensaoAmax) {
        this.tensaoAmax = tensaoAmax;
    }

    public double getTensaoAmin() {
        return tensaoAmin;
    }

    public void setTensaoAmin(double tensaoAmin) {
        this.tensaoAmin = tensaoAmin;
    }

    public double getTensaoB() {
        return tensaoB;
    }

    public void setTensaoB(double tensaoB) {
        this.tensaoB = tensaoB;
    }

    public double getTensaoBmax() {
        return tensaoBmax;
    }

    public void setTensaoBmax(double tensaoBmax) {
        this.tensaoBmax = tensaoBmax;
    }

    public double getTensaoBmin() {
        return tensaoBmin;
    }

    public void setTensaoBmin(double tensaoBmin) {
        this.tensaoBmin = tensaoBmin;
    }

    public double getTensaoC() {
        return tensaoC;
    }

    public void setTensaoC(double tensaoC) {
        this.tensaoC = tensaoC;
    }

    public double getTensaoCmax() {
        return tensaoCmax;
    }

    public void setTensaoCmax(double tensaoCmax) {
        this.tensaoCmax = tensaoCmax;
    }

    public double getTensaoCmin() {
        return tensaoCmin;
    }

    public void setTensaoCmin(double tensaoCmin) {
        this.tensaoCmin = tensaoCmin;
    }
}
