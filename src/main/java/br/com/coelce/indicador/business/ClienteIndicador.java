/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.coelce.indicador.business;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.wimpi.modbus.ModbusException;
import net.wimpi.modbus.io.ModbusTransaction;
import net.wimpi.modbus.msg.ReadDataTimeRequest;
import net.wimpi.modbus.msg.ReadDateTimeResponse;
import net.wimpi.modbus.msg.ReadInputRegistersRequest;
import net.wimpi.modbus.msg.ReadInputRegistersResponse;
import net.wimpi.modbus.msg.ReadMedidaRequest;
import net.wimpi.modbus.msg.ReadMedidasResponse;
import net.wimpi.modbus.msg.WriteDataTimeRequest;
import net.wimpi.modbus.msg.WriteDateTimeResponse;
import net.wimpi.modbus.msg.WriteSingleRegisterRequest;
import net.wimpi.modbus.msg.WriteSingleRegisterResponse;
import net.wimpi.modbus.net.Connection;
import net.wimpi.modbus.procimg.InputRegister;
import net.wimpi.modbus.procimg.MedicaoRegister;
import net.wimpi.modbus.procimg.SimpleInputRegister;
import net.wimpi.modbus.procimg.SimpleRegister;
import org.apache.commons.math.MathException;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.apache.commons.math.analysis.interpolation.LinearInterpolator;
import org.apache.commons.math.analysis.interpolation.UnivariateRealInterpolator;

/**
 *
 * @author pr02nl
 */
public abstract class ClienteIndicador {

    private int unitID;
    private byte calibrado;
    private byte medindo;
    private int countRegisters;
    public static int CONTADORES_A = 0x4B;
    public static int CONTADORES_B = 0x57;
    public static int CONTADORES_C = 0x63;
    public static int CALIBRADO = 0x6F;
    public static int CALIBRAR = 0x72;
    public static int MEDIR = 0x73;
    public static int MIN_V_FASE = 0x01;
    public static int MIN_V_FASE_B = 0x19;
    public static int MIN_V_FASE_C = 0x31;
    public static int Quant_MedL = 0x23;
    public static final int TENSAO_BASE = 0x88;
    public static final int ID_INDICADOR = 0x77;
    public static final int RADIO_BLUETOOTH = 1026;
    private final double f[] = {50, 100, 180, 189, 201, 231, 233, 250};
    private final double f127[] = {50, 60, 80, 109, 116, 133, 140, 250};
    private double xA[] = new double[f.length];
    private double xB[] = new double[f.length];
    private double xC[] = new double[f.length];
    private final int mask[] = {0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80};
    private final int memoria[] = {0, 1, 2, 3, 4, 5, 6, 7};
    private UnivariateRealFunction functionA;
    private UnivariateRealFunction functionB;
    private UnivariateRealFunction functionC;
    private int tensaoBase = 0;

    public abstract Connection getConnection();

    public abstract void setConnection(Connection con);

    public abstract ModbusTransaction getTransaction();

    public abstract void setTransaction(ModbusTransaction trans);

    public int getTensaoBase() {
        return tensaoBase;
    }

    public void setTensaoBase(int tensaoBase) {
        this.tensaoBase = tensaoBase;
    }

    public int calibrar(int index) throws ModbusException {
        WriteSingleRegisterRequest req = new WriteSingleRegisterRequest(CALIBRAR, new SimpleRegister(index));
        req.setUnitID(unitID);
        if (getConnection().isOpen()) {
            getTransaction().setRequest(req);
            getTransaction().execute();
            WriteSingleRegisterResponse response = (WriteSingleRegisterResponse) getTransaction().getResponse();
            return (response.getReference() << 16) | response.getRegisterValue();
        }
        return -1;
    }

    public int writeRegister(int index, int valor) throws ModbusException {
        WriteSingleRegisterRequest req = new WriteSingleRegisterRequest(index, new SimpleRegister(valor));
        req.setUnitID(unitID);
        if (getConnection().isOpen()) {
            getTransaction().setRequest(req);
            getTransaction().execute();
            WriteSingleRegisterResponse response = (WriteSingleRegisterResponse) getTransaction().getResponse();
//            return (response.getReference() << 16) | response.getRegisterValue();
            return response.getRegisterValue();
        }
        return -1;
    }

    public int[] lerMemoria(int index, int count) throws ModbusException {
        ReadInputRegistersRequest req = new ReadInputRegistersRequest(index, count);
        req.setUnitID(unitID);
        if (getConnection().isOpen()) {
            int[] registros = new int[count / 2];
            getTransaction().setRequest(req);
            getTransaction().execute();
            ReadInputRegistersResponse response = (ReadInputRegistersResponse) getTransaction().getResponse();
            for (int i = 0; i < response.getRegisters().length; i++) {
                byte[] toBytes = response.getRegister(i).toBytes();
                InputRegister input = new SimpleInputRegister(toBytes[1], toBytes[0]);
                registros[i] = input.getValue();
            }
            return registros;
        }
        return null;
    }

    public MedicaoRegister lerTensao(int index, int count) throws ModbusException {
        ReadMedidaRequest req = new ReadMedidaRequest(index, count);
        req.setUnitID(unitID);
        if (getConnection().isOpen()) {
            getTransaction().setRequest(req);
            getTransaction().execute();
            ReadMedidasResponse response = (ReadMedidasResponse) getTransaction().getResponse();
            return response.getRegister();

        }
        return null;
    }

    public Date lerData() throws ModbusException {
        ReadDataTimeRequest req = new ReadDataTimeRequest();
        req.setUnitID(unitID);
        if (getConnection().isOpen()) {
            getTransaction().setRequest(req);
            getTransaction().execute();
            ReadDateTimeResponse response = (ReadDateTimeResponse) getTransaction().getResponse();
            return response.getRegister();
        }
        return null;
    }

    public Date setData(Date data, int agendar) throws ModbusException {
        WriteDataTimeRequest req = new WriteDataTimeRequest(data, agendar);
        req.setUnitID(unitID);
        if (getConnection().isOpen()) {
            getTransaction().setRequest(req);
            getTransaction().execute();
            WriteDateTimeResponse response = (WriteDateTimeResponse) getTransaction().getResponse();
            return response.getRegister();
        }
        return null;
    }

    public Date setData() throws ModbusException {
        WriteDataTimeRequest req = new WriteDataTimeRequest(Calendar.getInstance().getTime());
        req.setUnitID(unitID);
        if (getConnection().isOpen()) {
            getTransaction().setRequest(req);
            getTransaction().execute();
            WriteDateTimeResponse response = (WriteDateTimeResponse) getTransaction().getResponse();
            return response.getRegister();
        }
        return null;
    }

    public void updateRegistros() throws ModbusException {
        ReadInputRegistersRequest req = new ReadInputRegistersRequest();
        ReadInputRegistersResponse resp;

        req.setReference(MIN_V_FASE);
        req.setWordCount(3 * 8);
        req.setUnitID(unitID);
        getTransaction().setRequest(req);
        getTransaction().execute();
        resp = (ReadInputRegistersResponse) getTransaction().getResponse();
        InputRegister[] registers = resp.getRegisters();
        int buffer[] = new int[resp.getByteCount()];
        int i = 0;
        for (int j = 0; j < registers.length; j++) {
            InputRegister inputRegister = registers[j];
            buffer[i++] = inputRegister.toBytes()[0] & 0xFF;
            buffer[i++] = inputRegister.toBytes()[1] & 0xFF;
        }
        i = 0;
        int aux;
        for (int j = 0; j < buffer.length - 2; j += 3) {
            aux = buffer[j];
            aux |= buffer[j + 1] << 8;
            aux |= buffer[j + 2] << 16;
            xA[i] = aux;
            i++;
        }
        System.out.println("x = " + Arrays.toString(xA));

        req.setReference(MIN_V_FASE_B);
        req.setWordCount(3 * 8);
        req.setUnitID(unitID);
        getTransaction().setRequest(req);
        getTransaction().execute();
        resp = (ReadInputRegistersResponse) getTransaction().getResponse();
        registers = resp.getRegisters();
        i = 0;
        for (int j = 0; j < registers.length; j++) {
            InputRegister inputRegister = registers[j];
            buffer[i++] = inputRegister.toBytes()[0] & 0xFF;
            buffer[i++] = inputRegister.toBytes()[1] & 0xFF;
        }
        i = 0;
        for (int j = 0; j < buffer.length - 2; j += 3) {
            aux = buffer[j];
            aux |= buffer[j + 1] << 8;
            aux |= buffer[j + 2] << 16;
            xB[i] = aux;
            i++;
        }
        System.out.println("x = " + Arrays.toString(xB));

        req.setReference(MIN_V_FASE_C);
        req.setWordCount(3 * 8);
        req.setUnitID(unitID);
        getTransaction().setRequest(req);
        getTransaction().execute();
        resp = (ReadInputRegistersResponse) getTransaction().getResponse();
        registers = resp.getRegisters();
        i = 0;
        for (int j = 0; j < registers.length; j++) {
            InputRegister inputRegister = registers[j];
            buffer[i++] = inputRegister.toBytes()[0] & 0xFF;
            buffer[i++] = inputRegister.toBytes()[1] & 0xFF;
        }
        i = 0;
        for (int j = 0; j < buffer.length - 2; j += 3) {
            aux = buffer[j];
            aux |= buffer[j + 1] << 8;
            aux |= buffer[j + 2] << 16;
            xC[i] = aux;
            i++;
        }
        System.out.println("x = " + Arrays.toString(xC));
    }

    public boolean isConnected() {
        return getConnection() != null && getConnection().isOpen();
    }

    public void disconnect() throws IOException {
        if (getConnection() != null && getConnection().isOpen()) {
            getConnection().close();
        }
        setConnection(null);
    }

    public abstract int connect(int index) throws Exception;

    public int firstConect() throws Exception {
        ReadInputRegistersRequest req = new ReadInputRegistersRequest(CALIBRADO, 2);
        req.setUnitID(0);
        if (!getConnection().isOpen()) {
            getConnection().open();
        }
        getTransaction().setRequest(req);
        getTransaction().execute();
        ReadInputRegistersResponse resp = (ReadInputRegistersResponse) getTransaction().getResponse();
        unitID = resp.getUnitID();
        calibrado = resp.getRegister(0).toBytes()[0];
        if (calibrado == 0) {
            req.setReference(TENSAO_BASE);
            getTransaction().execute();
            resp = (ReadInputRegistersResponse) getTransaction().getResponse();
            tensaoBase = resp.getRegister(0).toBytes()[0];
            updateRegistros();
        }
        getMedindo();
        return calibrado;
    }

    public int getCountRegisters() throws ModbusException {
        ReadInputRegistersRequest req = new ReadInputRegistersRequest(0x49, 2);
        req.setUnitID(unitID);
        getTransaction().setRequest(req);
        getTransaction().execute();
        ReadInputRegistersResponse response = (ReadInputRegistersResponse) getTransaction().getResponse();
        countRegisters = response.getRegisterValue(0) >> 8;
        countRegisters |= (response.getRegisterValue(0) & 0x00FF) << 8;
        return countRegisters;
    }

    public byte getMedindo() throws ModbusException {
        ReadInputRegistersRequest req = new ReadInputRegistersRequest(MEDIR, 2);
        req.setReference(MEDIR);
        req.setWordCount(2);
        req.setUnitID(unitID);
        getTransaction().setRequest(req);
        getTransaction().execute();
        ReadInputRegistersResponse resp = (ReadInputRegistersResponse) getTransaction().getResponse();
        medindo = resp.getRegister(0).toBytes()[0];
        return medindo;
    }

    public List<MedicaoRegister> dumpMemoria() throws ModbusException {
        int quant;
        ReadInputRegistersRequest req = new ReadInputRegistersRequest(0x49, 2);
        req.setUnitID(unitID);
        if (getConnection().isOpen()) {
            getTransaction().setRequest(req);
            getTransaction().execute();
            ReadInputRegistersResponse response = (ReadInputRegistersResponse) getTransaction().getResponse();
            quant = response.getRegisterValue(0) >> 8;
            quant |= (response.getRegisterValue(0) & 0x00FF) << 8;
            System.out.println(String.format("Existem %d registros.", quant));
            List<MedicaoRegister> list = new ArrayList<MedicaoRegister>(quant);
            for (int i = 0; i < quant; i++) {
                MedicaoRegister lerTensao;
                try {
                    lerTensao = lerTensao(i, 0);
                    list.add(lerTensao);
                } catch (ModbusException ex) {
                    Logger.getLogger(ClientBluetooth.class.getName()).log(Level.SEVERE, null, ex);
                    i--;
                }
            }
            return list;
        }
        return null;
    }

    private UnivariateRealFunction getFunctionA() throws MathException {
        if (functionA == null) {
            UnivariateRealInterpolator interpolator = new LinearInterpolator();
            functionA = interpolator.interpolate(xA, getF());
        }
        return functionA;
    }

    private UnivariateRealFunction getFunctionB() throws MathException {
        if (functionB == null) {
            UnivariateRealInterpolator interpolator = new LinearInterpolator();
            functionB = interpolator.interpolate(xB, getF());
        }
        return functionB;
    }

    private UnivariateRealFunction getFunctionC() throws MathException {
        if (functionC == null) {
            UnivariateRealInterpolator interpolator = new LinearInterpolator();
            functionC = interpolator.interpolate(xC, getF());
        }
        return functionC;
    }

    public double calculaTensaoA(int interpolationX) throws MathException {
        double x;
        if (interpolationX < xA[0]) {
            return 0;
        } else if (interpolationX > xA[xA.length - 1]) {
            x = xA[xA.length - 1];
        } else {
            x = interpolationX;
        }
        return getFunctionA().value(x);
    }

    public double calculaTensaoB(int interpolationX) throws MathException {
        double x;
        if (interpolationX < xB[0]) {
            return 0;
        } else if (interpolationX > xB[xB.length - 1]) {
            x = xB[xB.length - 1];
        } else {
            x = interpolationX;
        }
        return getFunctionB().value(x);
    }

    public double calculaTensaoC(int interpolationX) throws MathException {
        double x;
        if (interpolationX < xC[0]) {
            return 0;
        } else if (interpolationX > xC[xC.length - 1]) {
            x = xC[xC.length - 1];
        } else {
            x = interpolationX;
        }
        return getFunctionC().value(x);
    }

    public double[] getF() {
        if (tensaoBase == 220) {
            return f;
        } else {
            return f127;
        }
    }

    public int[] getMemoria() {
        return memoria;
    }

    public byte getCalibrado() {
        return calibrado;
    }

    public void setCalibrado(byte calibrado) {
        this.calibrado = calibrado;
    }

    public int[] getMask() {
        return mask;
    }
}
