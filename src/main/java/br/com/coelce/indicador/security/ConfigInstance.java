/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.coelce.indicador.security;

import br.com.coelce.indicador.model.CentroServico;
import br.com.coelce.indicador.model.Departamento;
import javax.inject.Singleton;

/**
 *
 * @author pr02nl
 */
@Singleton
public class ConfigInstance {

    private Departamento departamento;
    private CentroServico centroServico;

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public CentroServico getCentroServico() {
        return centroServico;
    }

    public void setCentroServico(CentroServico centroServico) {
        this.centroServico = centroServico;
    }
}
