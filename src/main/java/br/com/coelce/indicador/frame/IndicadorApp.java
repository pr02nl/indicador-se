/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.coelce.indicador.frame;

import br.com.coelce.indicador.facade.IndicadorFacade;
import br.com.coelce.indicador.persistence.DatabaseServer;
import br.gov.frameworkdemoiselle.annotation.Name;
import br.gov.frameworkdemoiselle.util.ResourceBundle;
import java.util.*;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import org.openswing.swing.domains.java.Domain;
import org.openswing.swing.internationalization.java.BrazilianPortugueseOnlyResourceFactory;
import org.openswing.swing.internationalization.java.Language;
import org.openswing.swing.lookup.client.LookupController;
import org.openswing.swing.mdi.client.ClientFacade;
import org.openswing.swing.mdi.client.MDIController;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.mdi.java.ApplicationFunction;
import org.openswing.swing.permissions.client.LoginController;
import org.openswing.swing.tree.java.OpenSwingTreeNode;
import org.openswing.swing.util.client.ClientSettings;
import org.openswing.swing.util.java.Consts;
import org.slf4j.Logger;

/**
 *
 * @author Paulo Roberto
 */
@Singleton
public class IndicadorApp implements MDIController, LoginController {

    @Inject
    private ResourceBundle bundle;
    @Inject
    @Name("messages-core")
    private ResourceBundle bundleCore;
    @Inject
    private IndicadorFacade analiseFacade;
    @Inject
    private Logger logger;
    @Inject
    private DatabaseServer databaseServer;

    @PostConstruct
    public void init() {
        Properties dictionary = new Properties();
        HashMap domains = new HashMap();
        Domain bolDomain = new Domain("BOOLEAN");
        bolDomain.addDomainPair(Boolean.FALSE, "false");
        bolDomain.addDomainPair(Boolean.TRUE, "true");
        domains.put(bolDomain.getDomainId(), bolDomain);
        Enumeration<String> keys = bundleCore.getKeys();
        while (keys.hasMoreElements()) {
            String key = keys.nextElement();
            String string = bundleCore.getString(key);
            dictionary.put(key, string);
        }

        ClientSettings clientSettings = new ClientSettings(
                new BrazilianPortugueseOnlyResourceFactory(dictionary, true),
                domains);
        dictionary = clientSettings.getResources().getDictionary();
        dictionary.setProperty(Consts.EQ, "Igual a");
        dictionary.setProperty(Consts.GE, "Maior ou igual a");
        dictionary.setProperty(Consts.GT, "Maior do que");
        dictionary.setProperty(Consts.IS_NOT_NULL, "Não é nulo");
        dictionary.setProperty(Consts.IS_NULL, "É nulo");
        dictionary.setProperty(Consts.LE, "Menor ou igual a");
        dictionary.setProperty(Consts.LIKE, "Contém");
        dictionary.setProperty(Consts.LT, "Menor que");
        dictionary.setProperty(Consts.NEQ, "Não é igual a");
        dictionary.setProperty(Consts.IN, "Contém valores");
        dictionary.setProperty(Consts.ASC_SORTED, "Crescente");
        dictionary.setProperty(Consts.DESC_SORTED, "Decrescente");
        dictionary.setProperty(Consts.NOT_IN, "Não contém valores");
        //ClientSettings.FILTER_PANEL_ON_GRID = true;
        ClientSettings.SHOW_FILTER_SYMBOL = true;
        ClientSettings.LOOK_AND_FEEL_CLASS_NAME = UIManager.getCrossPlatformLookAndFeelClassName();
        ClientSettings.LOOKUP_FRAME_CONTENT = LookupController.GRID_AND_FILTER_FRAME;
        ClientSettings.STORE_INTERNAL_FRAME_PROFILE = true;
        ClientSettings.BUTTON_BEHAVIOR = Consts.BUTTON_IMAGE_AND_TEXT;
        ClientSettings.BUTTONS_EXECUTE_AS_THREAD = true;
        databaseServer.start();
    }

    @Override
    public void afterMDIcreation(MDIFrame mdif) {
    }

    @Override
    public String getAboutImage() {
        return "about.jpg";
    }

    @Override
    public String getAboutText() {
        return bundle.getString("IndicadorApp.aboutText");
    }

    @Override
    public DefaultTreeModel getApplicationFunctions() {
        DefaultMutableTreeNode root = new OpenSwingTreeNode();
        DefaultTreeModel model = new DefaultTreeModel(root);
        ApplicationFunction indicador = new ApplicationFunction(bundle.getString("IndicadorApp.menu.indicador"), null);
        ApplicationFunction banco = new ApplicationFunction(bundle.getString("IndicadorApp.menu.banco"), null);
        indicador.add(new ApplicationFunction(bundle.getString("IndicadorApp.menu.indicador.buscar"), "showSearchFrame", null, "showSearchFrame", null));
        indicador.add(new ApplicationFunction(bundle.getString("IndicadorApp.menu.indicador.serial"), "showSerialFrame", null, "showSerialFrame", null));
        //principal.add(new ApplicationFunction(bundle.getString("IndicadorApp.menu.indicador.ver"), "showAnalistaView", null, "showAnalistaView", null));
        banco.add(new ApplicationFunction(bundle.getString("IndicadorApp.menu.banco.indicadores"), "showIndicadoresFrame", null, "showIndicadoresFrame", null));
        //principal.add(new ApplicationFunction(bundle.getString("IndicadorApp.menu.projetistas"), "showProjetistaView", null, "showProjetistaView", null));
        root.add(indicador);
        root.add(banco);
        return model;
    }

    @Override
    public ClientFacade getClientFacade() {
        return analiseFacade;
    }

    @Override
    public int getExtendedState() {
        return JFrame.MAXIMIZED_BOTH;
    }

    @Override
    public ArrayList getLanguages() {
        ArrayList list = new ArrayList();
        list.add(new Language("PT_BR", "Português"));
        return list;
    }

    @Override
    public String getMDIFrameTitle() {
        return bundle.getString("IndicadorApp.title");
    }

    @Override
    public void stopApplication() {
        System.exit(0);
    }

    @Override
    public boolean viewChangeLanguageInMenuBar() {
        return false;
    }

    @Override
    public boolean viewFileMenu() {
        return true;
    }

    @Override
    public boolean viewFunctionsInMenuBar() {
        return true;
    }

    @Override
    public boolean viewFunctionsInTreePanel() {
        return false;
    }

    @Override
    public JDialog viewLoginDialog(JFrame jframe) {
        return null;
    }

    @Override
    public boolean viewLoginInMenuBar() {
        return false;
    }

    @Override
    public boolean viewOpenedWindowIcons() {
        return true;
    }

    @Override
    public boolean authenticateUser(Map map) throws Exception {
        return true;
    }

    @Override
    public int getMaxAttempts() {
        return 0;
    }

    @Override
    public void loginSuccessful(Map map) {
    }
}
