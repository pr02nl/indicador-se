/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.coelce.indicador.exceptions;

/**
 *
 * @author Paulo Roberto
 */
public class ValidationException extends RuntimeException {

    public ValidationException(String string) {
        super(string);
    }
}
